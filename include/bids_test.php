<?php
// the following is copied from /usr/home/installbrain.com/reporting/config.php , when I'll upload the file i'll just include this config instead probably
$config_admin_installs = array("infospace", "bandoo", "babylon", "claro", "conduit",  "babylon_old", "babylon_old2", "babylon_old3", "findamo", "yahoo", "yandex", "hola", "yahoo_old", 'tika', 'csp', 'bueno', 'searchalgo');

$config_special_cpa = array();
$config_special_cpa[3901] = array('components' => array('cpa'=>'cpa', 'pcp'=>'pcperformer'), 'components_revenues' => array('pcperformer_revenues'));
$config_special_cpa[3935] = array('components' => array('cpa'=>'cpa', 'pcp'=>'pcperformer'), 'components_revenues' => array('pcperformer_revenues'));
$config_special_cpa[3942] = array('components' => array('cpa'=>'cpa', 'pcp'=>'pcperformer'), 'components_revenues' => array('pcperformer_revenues'));
$config_special_cpa[3948] = array('components' => array('cpa'=>'cpa', 'pcp'=>'pcperformer'), 'components_revenues' => array('pcperformer_revenues'));
$config_special_cpa[4131] = array('components' => array('cpa'=>'cpa', 'speedanalysis4'=>'speedanalysiscpa'), 'components_revenues' => array('speedanalysiscpa_revenues'));
$config_special_cpa[4270] = array('components' => array('cpa'=>'cpa', '7go02'=>'7go02cpa'), 'components_revenues' => array('7go02cpa_revenues'));
$config_special_cpa[4281] = array('components' => array('cpa'=>'cpa', '7go03'=>'7go03cpa'), 'components_revenues' => array('7go03cpa_revenues'));
$config_special_cpa[4327] = array('components' => array('cpa'=>'cpa', 'zula02'=>'zula02cpa'), 'components_revenues' => array('zula02cpa_revenues'));
$config_special_cpa[4328] = array('components' => array('cpa'=>'cpa', 'speedanalysis7'=>'speedanalysis7cpa'), 'components_revenues' => array('speedanalysis7cpa_revenues'));
$config_special_cpa[4350] = array('components' => array('cpa'=>'cpa', 'speedtest4350'=>'speedanalysis4350cpa'), 'components_revenues' => array('speedanalysis4350cpa_revenues'));
$config_special_cpa[4352] = array('components' => array('cpa'=>'cpa', 'speedtest4352'=>'speedanalysis4352cpa'), 'components_revenues' => array('speedanalysis4352cpa_revenues'));
$config_special_cpa[4353] = array('components' => array('cpa'=>'cpa', 'speedtest4353'=>'speedanalysis4353cpa'), 'components_revenues' => array('speedanalysis4353cpa_revenues'));
//$config_special_cpa[4354] = array('components' => array('speedtest4354'=>'speedanalysis4354cpa'), 'components_revenues' => array('speedanalysis4354cpa_revenues'));
$config_special_cpa[4355] = array('components' => array('cpa'=>'cpa', 'speedtest4355'=>'speedanalysis4355cpa'), 'components_revenues' => array('speedanalysis4355cpa_revenues'));
$config_special_cpa[4356] = array('components' => array('cpa'=>'cpa', 'speedtest4356'=>'speedanalysis4356cpa'), 'components_revenues' => array('speedanalysis4356cpa_revenues'));
$config_special_cpa[4641] = array('components' => array('cpa'=>'cpa', 'pcp'=>'pcperformer'), 'components_revenues' => array('pcperformer_revenues'));
$config_special_cpa[4339] = array('components' => array('cpa'=>'cpa', 'freegames4339'=>'freegames4339cpa'), 'components_revenues' => array('freegames4339cpa_revenues'));
$config_special_cpa[4351] = array('components' => array('cpa'=>'cpa', 'freegames4351'=>'freegames4351cpa'), 'components_revenues' => array('freegames4351cpa_revenues'));
//$config_special_cpa[4357] = array('components' => array('freegames4357'=>'freegames4357cpa'), 'components_revenues' => array('freegames4357cpa_revenues'));
$config_special_cpa[4358] = array('components' => array('cpa'=>'cpa', 'freegames4358'=>'freegames4358cpa'), 'components_revenues' => array('freegames4358cpa_revenues'));
$config_special_cpa[4359] = array('components' => array('cpa'=>'cpa', 'freegames4359'=>'freegames4359cpa'), 'components_revenues' => array('freegames4359cpa_revenues'));
$config_special_cpa[4360] = array('components' => array('cpa'=>'cpa', 'freegames4360'=>'freegames4360cpa'), 'components_revenues' => array('freegames4360cpa_revenues'));
$config_special_cpa[4361] = array('components' => array('cpa'=>'cpa', 'freegames4361'=>'freegames4361cpa'), 'components_revenues' => array('freegames4361cpa_revenues'));
$config_special_cpa[4397] = array('components' => array('cpa'=>'cpa', 'freegames4397'=>'freegames4397cpa'), 'components_revenues' => array('freegames4397cpa_revenues'));

// Build an array with special_cpa components
$config_special_cpa_components = $config_special_cpa_components_revenues = array();
foreach ($config_special_cpa as $cpa_cid => $cpa_details)
{
	foreach ($cpa_details['components'] as $cpa_components)
	{
		if (!in_array($cpa_components, $config_special_cpa_components)) $config_special_cpa_components[] = $cpa_components;
	}
	foreach ($cpa_details['components_revenues'] as $cpa_components)
	{
		if (!in_array($cpa_components, $config_special_cpa_components_revenues)) $config_special_cpa_components_revenues[] = $cpa_components;
	}
}
// the following is copied from /usr/home/ibario.com/tools/common_functions.php , when I'll upload the file i'll just include this config instead probably
?>