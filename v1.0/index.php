<?php
// change_bids_test.php - test script that tries to modify bids in a specific campaign based on data from reporting & data from adwords account (bid etc.)
// ====================================================================================================================================================================================
require_once "../includes/adwordsapi.class.php";                                // adwords class that is used for operations using Adwords API on the MCC account
require_once "../includes/meekrodb.2.2.class.php";                              // meekro class that is used for DB operations
require_once "include/definitions.php";                                         // definitions.php - an include file that contains definitions that are specific for this script
$adwords = new adwordsapi();
$local_db = new MeekroDB("localhost", "root", "Miki123Abc", "adwords_bidder");
// ====================================================================================================================================================================================
$op = isset($_GET['op'])?$_GET['op']:"select";
switch ($op) {
    case "select":              include_once "select.php";              break;  // if no "op" value - bring us back to the first screen which is the selector
    case "chosecid":            include_once "chose_cid.php";           break;  // second screen - after we chose cid
    case "collectadwords":      include_once "collect_adwords.php";     break;  // the script used to collect data from google adwords
    case "collectreporting":    include_once "collect_reporting.php";   break;  // the script used to collect data from reporting.installbrain.com system
    case "collectlog":          include_once "collect_log.php";         break;  // the script used to show the collect log
    case "measure":             include_once "measure.php";             break;  // the script that does the measuring - does country breakdown ordered by spend and notifies us
    case "prompt":              include_once "prompt.php";              break;  // prompt the changes that are going to be done now..
    case "change":              include_once "change.php";              break;  // allow to make last minute changes before modifying
    case "changebids":          include_once "change_bids.php";         break;  // do the actual changes to the bids
    case "updatebids":          include_once "update_bids.php";         break;  // do the actual changes to the bids
}
?>