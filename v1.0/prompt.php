<?php
/*
	A. Calculate Value (Revenue / Pixels)
	B. Calculate CPA (Spend / Pixels)
	C. Set the new bid value for the adgroups on the campaigns of the chosen geo
		1. Calculate the value for the new bid
			A. if campaign is a DCO campaign (contains "DCO" in its name) 	=> New Bid = Value / 1.25
			B. If campaign is content/keyword 				=> New Bid = Value / 1.30
		2. Compare the new bid against the old bid that is currently on the adgroups
			A. If campaign is a DCO campaign 				=> The maximum allowed change to the bid is no more than 10% up or down
			B. If campaign is content/keyword				=> The maximum allowed change to the bid is 0.35$ up or down
*/
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$cid = $_GET['cid'];                                                            // get the CID information (client_id / name)
$minimum = $_GET['minimum'];                                                    // get the minimum spend to look up
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_collected_reporting_data($cid,$minimum) {
    global $local_db;
    $output = array();
    $data = $local_db->query("SELECT * FROM reporting_data WHERE cid=$cid AND spend>$minimum ORDER BY spend DESC");
    return $data;
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_changes_array($data) {
    $output = array();
    foreach ($data as $i=>$array) {
        $country = $array['country'];
        $pixels = $array['pixels'];
        $revenue = $array['revenue'];
        $spend = $array['spend'];
        $value = number_format($revenue / $pixels, 2);
        $cpa = number_format($spend / $pixels, 2);
        $output[$country] = array("value"=>$value, "cpa"=>$cpa, "revenue"=>$revenue, "spend"=>$spend, "pixels"=>$pixels);
    }
    return $output;
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_campaigns($client, $country) {
    global $local_db;
    $client_id = $client['client_id'];
    $campaigns = $local_db->query( "SELECT a.*,b.period,b.amount,b.bidding_strategy FROM campaigns a
                                    INNER JOIN budgets b ON b.budget_id=a.budget_id
                                    WHERE a.client_id=$client_id AND a.status='ACTIVE' AND a.geo_location='$country'");
    return $campaigns;
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_campaigns_country(&$changes, $client) {
    foreach ($changes as $country=>$array) {
        $campaigns = get_campaigns($client, $country);
        $changes[$country]['campaigns'] = $campaigns;
    }
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_adgroups($client, $campaign_id) {
    global $local_db;
    $client_id = $client['client_id'];
    $adgroups = $local_db->query( "SELECT * FROM adgroups WHERE client_id=$client_id AND campaign_id=$campaign_id AND status='ENABLED' AND bid_type='TARGET_CPA'" );
    return $adgroups;
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_adgroups_country(&$changes, $client) {
    foreach ($changes as $country=>$array) {
        $campaigns = $array['campaigns'];
        foreach ($campaigns as $i=>$campaign) {
            $adgroups = get_adgroups($client, $campaign['campaign_id']);
            $changes[$country]['campaigns'][$i]['adgroups'] = $adgroups;
        }
    }
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function calculate_dco_bid_change($old_bid) {
    if ($old_bid<=1500000) return intval($old_bid * 0.1);                       // if less than 1.5$    = 10%
    if (($old_bid>1500000)&&($old_bid<=2000000)) return intval($old_bid * 0.08);// if between 1.5$ - 2$ = 8%
    if (($old_bid>2000000)&&($old_bid<=2500000)) return intval($old_bid * 0.05);// if between 2$ - 2.5$ = 5%
    if ($old_bid>2500000) return intval($old_bid * 0.03);                       // if more than 2.5$    = 3%
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function dco_bid_change(&$changes, $country, $campaign_number) {
    $array = $changes[$country];
    $campaign = $array['campaigns'][$campaign_number];
    $adgroups = $campaign['adgroups'];
    $revenue = $array['revenue'];
    $pixels = $array['pixels'];
    $value = $revenue / $pixels;                                                // calculate value
    foreach ($adgroups as $i=>$adgroup) {                                       // update the new bid in the adgroups array
        $new_bid = $value / 1.25;                                               // new bid for DCO campaigns should be Value / 1.25
        $new_bid = intval($new_bid * 1000000);                                  // convert the new_bid value to google money (millions)
        $old_bid = $changes[$country]['campaigns'][$campaign_number]['adgroups'][$i]['bid'];
        $changes[$country]['campaigns'][$campaign_number]['adgroups'][$i]['new_bid'] = $new_bid;
        $diff = $new_bid - $old_bid;                                            // check the difference between the old bid and new bid
        //$tenth = intval($old_bid/10);                                           // calculate 10% of the old_bid
        $tenth = calculate_dco_bid_change($old_bid);
        if (abs($diff)>$tenth) $diff = ($diff<0)?-$tenth:$tenth;                // on DCO campaigns - maximum change is 10% up or down
        $actual_bid = $old_bid + $diff;
        $changes[$country]['campaigns'][$campaign_number]['adgroups'][$i]['bid_ratio'] = 1.25;
        $changes[$country]['campaigns'][$campaign_number]['adgroups'][$i]['diff'] = $diff;
        $changes[$country]['campaigns'][$campaign_number]['adgroups'][$i]['actual_bid'] = $actual_bid;
    }
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function calculate_content_bid_change($old_bid) {
    if ($old_bid<1000000) return intval($old_bid * 0.2);                        // if less than 1$      = 20%
    if (($old_bid>1000000)&&($old_bid<=1500000)) return intval($old_bid * 0.15);// if between 1$ - 1.5$ = 15%
    if (($old_bid>1500000)&&($old_bid<=2000000)) return intval($old_bid * 0.12);// if between 1.5$ - 2$ = 12%
    if (($old_bid>2000000)&&($old_bid<=2500000)) return intval($old_bid * 0.10);// if between 2$ - 2.5$ = 10%
    if ($old_bid>2500000) return intval($old_bid * 0.08);                       // if more than 2.5$    = 8%
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function content_bid_change(&$changes, $country, $campaign_number) {
    $array = $changes[$country];
    $campaign = $array['campaigns'][$campaign_number];
    $adgroups = $campaign['adgroups'];
    $revenue = $array['revenue'];
    $pixels = $array['pixels'];
    $value = $revenue / $pixels;                                                // calculate value
    foreach ($adgroups as $i=>$adgroup) {                                       // update the new bid in the adgroups array
        $new_bid = $value / 1.3;                                                // new bid for CONTENT campaigns should be Value / 1.3
        $new_bid = intval($new_bid * 1000000);                                  // convert the new_bid value to google money (millions)
        $old_bid = $changes[$country]['campaigns'][$campaign_number]['adgroups'][$i]['bid'];
        $changes[$country]['campaigns'][$campaign_number]['adgroups'][$i]['new_bid'] = $new_bid;
        $diff = $new_bid - $old_bid;                                            // check the difference between the old bid and new bid
        //$twenieth = intval($old_bid/5);                                         // calculate 20% of the old_bid
        $twenieth = calculate_content_bid_change($old_bid);
        if (abs($diff)>$twenieth) $diff = ($diff<0)?-$twenieth:$twenieth;       // on CONTENT campaigns - maximum change is 20% up or down
        $actual_bid = $old_bid + $diff;
        $changes[$country]['campaigns'][$campaign_number]['adgroups'][$i]['bid_ratio'] = 1.3;
        $changes[$country]['campaigns'][$campaign_number]['adgroups'][$i]['diff'] = $diff;
        $changes[$country]['campaigns'][$campaign_number]['adgroups'][$i]['actual_bid'] = $actual_bid;
    }
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function calculate_changes(&$changes) {
    foreach ($changes as $country=>$change) {
        $campaigns = $change['campaigns'];
        foreach ($campaigns as $n=>$campaign) {
            $campaign_name = $campaign['campaign_name'];
            if (strpos($campaign_name,"DCO")>0) {                               // if DCO is found in campaign name - calculate bids for DCO campaigns
                dco_bid_change($changes, $country, $n);
            } else {                                                            // else - calculate bids for CONTENT campaigns
                content_bid_change($changes, $country, $n);
            }
        }
    }
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_email($changes) {
    global $minimum, $cid;
    $mail = "<b>Showing changes that are made based on data for CID #$cid from ";
    $mail.= (($minimum==60)?"last week":"last month").":</b><br><br>";
    foreach ($changes as $country=>$change) {
        $mail .= "Geo location: $country<br>";
        $mail .= "Revenue: $".$change['revenue']."<br>";
        $mail .= "Spend: $".$change['spend']."<br>";
        $mail .= "Pixels: ".$change['pixels']."<br>";
        $mail .= "Value: <b>$".number_format($change['revenue']/$change['pixels'],2)."</b><br><br>";
        $campaigns = $change['campaigns'];
        foreach ($campaigns as $n=>$campaign) {
            $campaign_name = $campaign['campaign_name'];
            $mail .= "<u>Campaign: $campaign_name</u><br>";
            $mail .= "Budget: $".number_format($campaign['amount']/1000000,2)."<br>";
            $mail .= "Period: {$campaign['period']}<br>";
            $mail .= "Bidding Strategy: {$campaign['bidding_strategy']}<br>";
            $adgroups = $campaign['adgroups'];
            foreach ($adgroups as $i=>$adgroup) {
                $name = $adgroup['adgroup_name'];
                $bid_type = $adgroup['bid_type'];
                $old_bid = number_format($adgroup['bid']/1000000,2);
                $new_bid = number_format($adgroup['new_bid']/1000000,2);
                $diff = number_format($adgroup['diff']/1000000,2);
                $actual_bid = number_format($adgroup['actual_bid']/1000000,2);
                $ratio = $adgroup['bid_ratio'];
                $mail .= "Adgroup #$i: '$name' , bid_type=$bid_type , current_bid=$$old_bid , calculated_bid=$$new_bid , change_ratio=$ratio , difference=$$diff , actual_bid_set=<b>$$actual_bid</b><br>";
            }
            $mail .= "<br><br>";
        }
    }
    echo $mail;
    file_put_contents("changes_to_be_done.arr",serialize($changes));
    echo '<button onclick="location.href=\'index.php?op=change&cid='.$cid.'\'">Go to preview and final approve..</button>';
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$client = $cids[$cid];                                                          // get client_id information about the current CID
$data = get_collected_reporting_data($cid,$minimum);                            // get collected reporting data, look for all geos with spend of more than 60$ last week..
$changes = create_changes_array($data);                                         // create an array, based on country, that contains pixels, revenue, spend, cpa and value
get_campaigns_country($changes, $client);                                       // get campaigns for every geo and add them to the changes array
get_adgroups_country($changes, $client);                                        // get the adgroups for every geo and add them to the changes array
calculate_changes($changes);                                                    // calculate the changes needed to be done per geo / campaign
create_email($changes);                                                         // create the resulting email / html output based on data collected
?>
