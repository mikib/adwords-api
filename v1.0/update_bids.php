<?php
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$fw = 0;                                                                        // file handler
$cid = $_GET['cid'];                                                            // get the CID information (client_id / name)
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function start_log() { global $fw; $fw = fopen('collect.log', 'a'); }
function do_log($message) { global $fw; fwrite($fw,"$message\r\n"); }
function close_log() { global $fw; fclose($fw); }
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function update_bids($changes) {
    global $adwords;
    $adgroups_bids = array();
    foreach ($changes as $country=>$change) {
        $campaigns = $change['campaigns'];
        foreach ($campaigns as $n=>$campaign) {
            $adgroups = $campaign['adgroups'];
            foreach ($adgroups as $i=>$adgroup) {
                $amount = $adgroup['actual_bid'] / 1000000;                     // the new amount that we pass to the adwords class is in $'s
                $adgroups_bid = array("adgroup_id"=>$adgroup['adgroup_id'],"bid"=>"cpa","amount"=>$amount);
                $old_bid = number_format($adgroup['bid']/1000000,2);            // show the old bid for display
                $actual_bid = number_format($adgroup['actual_bid']/1000000,2);  // show the new bid that is going to be set - for display
                do_log("updating adgroup {$adgroup['adgroup_id']} ({$adgroup['adgroup_name']}) , current_bid=$old_bid, new_bid=$actual_bid..");
                $adgroups_bids[] = $adgroups_bid;                               // add this bid for a bulk update of bids - faster!
            }
        }
    }
    if ($adgroups_bids) $adwords->update_adgroups($adgroups_bids);              // call the adwords class update_adgroups function to do the actual new bids setup (bulk update)
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
start_log();                                                                    // start logging
$client = $cids[$cid];                                                          // get client_id information about the current CID
$adwords->switch_user($client['client_id']);                                    // change the user
do_log("adwords: switched user to {$client['client_id']}");
$changes = unserialize(file_get_contents("changes_to_be_done.arr"));            // reload the changes array from our file
do_log("loaded the changes array, start updating bids..");
update_bids($changes);                                                          // do the bids changes
do_log("deleting changes file");
unlink("changes_to_be_done.arr");                                               // delete the changes file
do_log("done!");
close_log();
?>
