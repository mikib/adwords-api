<?php
// ================================================================================================================================================================================
$cids = array(                                                                  // $cids = array of predefined CIDs and their relative client_id / client_name from the MCC account
    "4258"  => array(
        "client_id"     => "6092654773",
        "client_name"   => "Free Codec"
    ),
    "4259"  => array(
        "client_id"     => "7207592165",
        "client_name"   => "PDF6"
    ),
    "4446"  => array(
        "client_id"     => "9036797234",
        "client_name"   => "ProntoZip"
    ),
    "4447"  => array(
        "client_id"     => "1456828957",
        "client_name"   => "UnzipHero"
    ),
    "4449"  => array(
        "client_id"     => "8386446563",
        "client_name"   => "PDFSwift"
    ),
    "4450"  => array(
        "client_id"     => "3231125763",
        "client_name"   => "ProntoReader"
    ),
    "4451"  => array(
        "client_id"     => "4130655277",
        "client_name"   => "BoosterPDF"
    ),
    "3868"  => array(
        "client_id"     => "5025189755",
        "client_name"   => "Speed"
    ),
    "4183"  => array(
        "client_id"     => "3635572580",
        "client_name"   => "ZoolaGames"
    ),
    "4606"  => array(
        "client_id"     => "3730003559",
        "client_name"   => "VideoConv"
    ),
    "4143"  => array(
        "client_id"     => "9000898534",
        "client_name"   => "UPDF"
    ),
    "3867"  => array(
        "client_id"     => "8094906163",
        "client_name"   => "77Zip"
    ),
    "4019"  => array(
        "client_id"     => "8310357514",
        "client_name"   => "Rocket"
    ),
    "4262"  => array(
        "client_id"     => "5184116516",
        "client_name"   => "Unzip xprs"
    ),
    "3683"  => array(
        "client_id"     => "7638468483",
        "client_name"   => "MDLM"
    ),
);
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$main_page = "index.php";                                                       // <-- the main page which we use to browse through the different stages
?>