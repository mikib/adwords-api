<?php
// select.php - this file is used on the first screen of the Adwords API optimizer - to choose a CID to work with

?>
<html>
<head>
<script type="text/javascript">
function go() {
    var select = document.getElementsByTagName('select')[0];
    var options = select.options;
    location.href = '<?=$main_page?>?op=chosecid&cid='+options[select.selectedIndex].value;
}
</script>
</head>
<body>
<table width="100%" height="100%">
<tr>
<td width="100%" height="100%" align="center" valign="middle">
  <table cellspacing="0" cellpadding="0">
    <tr>
      <td>
      <div style="float:left">Choose CID:</div>
      <div style="float:left;margin-left:10px">
        <select>
        <?php foreach ($cids as $key=>$cid) { ?>
          <option value="<?=$key?>"><?=$key?> - <?=$cid['client_name']?></option>
        <?php } ?>
        </select>
      </div>
      </td>
    </tr>
    <tr><td height="20"></td></tr>
    <tr><td align="center"><button onclick="go()">Go</button></td></tr>
  </table>
</td>
</tr>
</table>
</body>
</html>
