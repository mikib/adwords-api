<?php
// we chose CID.. now we need to do this according to the design
//2. Collect all the campaigns / adgroups data of the CID (Based on Client name) from google using adwords api
//3. Collect the reporting statistics from -8 days to -2 days for the desired CID (A week from 2 days ago)
if (file_exists("changes_to_be_done.arr")) unlink("changes_to_be_done.arr");
if (file_exists("collect.log")) unlink("collect.log");
?>
<html>
<head>
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
var stage=1;                                                                    // stage #1 = Adwords collection
var cid=<?=$_GET['cid']?>;
var interval=0;
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function refresh_collect_log() {                                                // this function reads the collect.log file generated on the server and displays its output on page
    $.ajax({
        type: "GET",
        url: "index.php?op=collectlog",                                         // the PHP script is called "collect_log.php" and it simply returns the collect.log data
        success: function(data){
            $("#status").html(data);                                            // put the received data in our "status" element (will simulate real-time updating)
            if (data.indexOf("done!")>0) {
                $("#img")[0].src = "images/Actions-dialog-ok-apply-icon.png";
                clearInterval(interval);
                switch (stage) {
                    // ============================================================================================================================================================
                    case 1: stage++;
                            setTimeout(reporting_collect,1000);
                            break;
                    // ============================================================================================================================================================
                    case 2: stage++;
                            setTimeout(measure,1000);
                            break;
                    // ============================================================================================================================================================
                    case 3: minimum = parseInt(data.substr(data.indexOf("done!")).match(/[0-9]+/g)[0]);         // extract the number (240) or (60) from the "done! (60)" message
                            location.href = '<?=$main_page?>?op=prompt&cid='+cid+'&minimum='+minimum;
                            break;
                }
            } else if (data.indexOf("error:")>=0) alert('Errors occured!');
        }
    });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function data_collect() {                                                       // this is the function we start the script with.  collecting adwords data on the server
    $("#status").html("");                                                      // clean the status log
    $.ajax({
        type: "GET",
        url: "index.php?op=collectadwords&cid="+cid,
    });
    interval = setInterval(refresh_collect_log,500);                            // start interval of 500ms that refreshes the log file (collect.log) that is created on the server
}
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function reporting_collect() {                                                  // this is the function we run after we finished adwords collection.  collect data from reporting
    $("#status").html("");                                                      // clean the status log
    $("#img")[0].src = "images/loading.gif";
    $("#msg").html("Collecting reporting data for CID #"+cid+" from the reporting system..");
    $.ajax({
        type: "GET",
        url: "index.php?op=collectreporting&cid="+cid,
    });
    interval = setInterval(refresh_collect_log,500);    
}
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function measure() {                                                            // this is the function we run after we finished data collection.  measure the data ..
    $("#status").html("");                                                      // clean the status log
    $("#img")[0].src = "images/loading.gif";
    $("#msg").html("Measuring collected data for CID #"+cid+"..");
    $.ajax({
        type: "GET",
        url: "index.php?op=measure&cid="+cid,
    });
    interval = setInterval(refresh_collect_log,500);    
}
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$(window).load(function(){data_collect()});
</script>
</head>
<body>
<table width="100%" height="100%">
    <tr>
        <td>
            <b id="msg">Collecting campaign data for CID #<?=$_GET['cid']?> from Google using Adwords..</b>
            <img id="img" src="images/loading.gif" style="width:20px;height:20px;position:relative;top:4px" />
        </td>
    </tr>
    <tr><td width="100%" height="100%" align="left" valign="top" id="status"></td></tr>
</table>
</body>
</html>