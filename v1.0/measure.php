<?php
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$fw = 0;
$cid = $_GET['cid'];                                                            // get the CID information (client_id / name)
$minimum = 60;                                                                  // minumum 60$ spend data we work on
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function start_log() { global $fw; $fw = fopen('collect.log', 'a'); }
function do_log($message) { global $fw; fwrite($fw,"$message\r\n"); }
function close_log() { global $fw; fclose($fw); }
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_collected_reporting_data($cid) {
    global $local_db,$minimum;
    $output = array();
    $data = $local_db->query("SELECT * FROM reporting_data WHERE cid=$cid AND spend>$minimum ORDER BY spend DESC");
    return $data;
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
start_log();
do_log("getting collected reporting data..");
$data = get_collected_reporting_data($cid);                                     // get collected reporting data, look for all geos with spend of more than 60$ last week..
if (!count($data)) {                                                            // if none are found.. then go 1 month back and look for geos with 240$ spend on whole month..
    do_log("not enough data, collecting data from 1 month ago..");
    include_once "collect_reporting_month.php";                                 // bring back last 1 month data
    $minimum = 240;
    $data = get_collected_reporting_data($cid);
    if (!count($data)) do_log("error: not enough data to perform operations");
}
do_log("done! ($minimum)");

?>