<?php                                                                           // this file is ONLY to be included by the "measure.php" script!
require_once "include/reporting_config.php";                                    // reporting_config.php - an include file that contains definitions for reporting (from config.php)
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$reporting_db = new MeekroDB(   "50.22.193.151",                                // $reporting_db = connection to the reporting DB
                                "ibario",
                                "ajsm28zjqhwa193",
                                "ibario"    );
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_channel_ids($cid) {
    global $reporting_db;
    $from_date = date("Y-m-d",time()-86400*7);
    $cid_totals = $reporting_db->query("SELECT * from cid_channels_totals WHERE cid=$cid AND createdate>='$from_date'");
    $channels = explode(",",$cid_totals[0]['channels']);
    return $channels;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_reporting_data($channels) {
    global $reporting_db,$config_admin_installs,$config_admin_revenues,$config_sales_components,$config_search_components,$config_secondary_components,$config_special_cpa_components,$config_special_cpa_components_revenues;
    $output = array();
    $from_date = date("Y-m-d",strtotime("-32 day"));
    $to_date = date("Y-m-d",strtotime("-2 day"));
    $query = "SELECT country,SUM(amount*factor/100) AS revenue FROM stats ".
             "WHERE codename NOT like '%revenues%' ".
             "AND codename IN ('".implode("','",$config_admin_revenues)."') ".
             "AND channel_id IN ('".implode("','",$channels)."') ".
             "AND createdate>='$from_date' AND createdate<='$to_date' GROUP by country";
    $data = $reporting_db->query($query);
    foreach ($data as $i=>$array) $output[$array['country']]['revenue'] = $array['revenue'];   // organize revenue sums by country (geo)
    $query = "SELECT country, SUM(clicks) AS clicks, SUM(installs) AS pixels, SUM(amount*factor/100) AS spend FROM stats ".
             "WHERE channel_id IN ('".(implode("','", $channels))."') ".
             "AND codename NOT IN ('".implode("','", $config_sales_components)."') ".
             "AND codename NOT IN ('".implode("','", $config_search_components)."') ".
             "AND codename NOT IN ('".implode("','", $config_secondary_components)."') ".
             "AND codename NOT IN ('".implode("','", $config_special_cpa_components)."') ".
             "AND codename NOT IN ('".implode("','", $config_special_cpa_components_revenues)."') ".
             "AND codename NOT LIKE '%revenues%' ".
             "AND createdate>='$from_date' AND createdate<='$to_date' GROUP by country";
    $data = $reporting_db->query($query);
    foreach ($data as $i=>$array) {
        $output[$array['country']]['clicks'] = $array['clicks'];
        $output[$array['country']]['pixels'] = $array['pixels'];
        $output[$array['country']]['spend'] = $array['spend'];
    }
    return $output;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function collect_config_admin($type) {
    global $reporting_db;
    $output = array();
    $data = $reporting_db->query("SELECT * FROM config_codenames_definition WHERE admin_type='$type'");
    foreach ($data as $i=>$arr) $output[] = $arr['codename'];
    return $output;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function collect_config_component($type) {
    global $reporting_db;
    $output = array();
    $data = $reporting_db->query("SELECT * FROM config_codenames_definition WHERE component_type='$type'");
    foreach ($data as $i=>$arr) $output[] = $arr['codename'];
    return $output;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function update_reporting($cid, $data) {
    global $local_db;
    $from_date = date("Y-m-d",strtotime("-32 day"));
    $to_date = date("Y-m-d",strtotime("-2 day"));
    $local_db->delete('reporting_data',"cid=$cid");                             // delete the budgets table to be sure we get ACCURATE budgets
    foreach ($data as $country=>$stats) {
        $local_db->insert("reporting_data",array(
            'cid'       => $cid,
            'from_date' => $from_date,
            'to_date'   => $to_date,
            'country'   => $country,
            'revenue'   => (isset($stats['revenue']))?$stats['revenue']:0,
            'spend'     => (isset($stats['spend']))?$stats['spend']:0,
            'clicks'    => (isset($stats['clicks']))?$stats['clicks']:0,
            'pixels'    => (isset($stats['pixels']))?$stats['pixels']:0
            ));
    }
}
// -----------------------------------------------------------------------------------------------------------------------------------------------> collect reporting data with CID
$config_admin_revenues = collect_config_admin('revenue');                       // configuration
$config_sales_components = collect_config_component('sales');                   // configuration
$config_search_components = collect_config_component('search');                 // configuration
$config_secondary_components = collect_config_component('secondary');           // configuration
$channel_ids = get_channel_ids($cid);
do_log("reporting: brought channel IDs");
$data = get_reporting_data($channel_ids);
do_log("reporting: brought reporting data of last 30 days");
// -----------------------------------------------------------------------------------------------------------------------------------------------> save reporting data into DB
do_log("reporting: saving to DB..");
update_reporting($cid,$data);
?>