<?php
// here we do the actual update process of the bids..
?>
<html>
<head>
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
var cid=<?=$_GET['cid']?>;
var interval=0;
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function refresh_collect_log() {                                                // this function reads the collect.log file generated on the server and displays its output on page
    $.ajax({
        type: "GET",
        url: "index.php?op=collectlog",                                         // the PHP script is called "collect_log.php" and it simply returns the collect.log data
        success: function(data){
            $("#status").html(data);                                            // put the received data in our "status" element (will simulate real-time updating)
            if (data.indexOf("done!")>0) {
                $("#img")[0].src = "images/Actions-dialog-ok-apply-icon.png";
                clearInterval(interval);
            } else if (data.indexOf("error:")>=0) alert('Errors occured!');
        }
    });
}
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function update_bids() {                                                        // this is the function we start the script with.  collecting adwords data on the server
    $("#status").html("");                                                      // clean the status log
    $.ajax({type:"GET", url:"index.php?op=updatebids&cid="+cid});               // do ajax call
    interval = setInterval(refresh_collect_log,500);                            // start interval of 500ms that refreshes the log file (collect.log) that is created on the server
}
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$(window).load(function(){update_bids()});
</script>
</head>
<body>
<table width="100%" height="100%">
    <tr>
        <td>
            <b id="msg">Updating bids for adgroups in campaign CID #<?=$_GET['cid']?>..</b>
            <img id="img" src="images/loading.gif" style="width:20px;height:20px;position:relative;top:4px" />
        </td>
    </tr>
    <tr><td width="100%" height="100%" align="left" valign="top" id="status"></td></tr>
</table>
</body>
</html>