<?php
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$fw = 0;                                                                        // file handler
$cid = $_GET['cid'];                                                            // get the CID information (client_id / name)
$changes = unserialize(file_get_contents("changes_to_be_done.arr"));            // reload the changes array from our file
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if (isset($_GET['newbid'])) {                                                   // if setting new bid manually..
    $geo = $_GET['geo'];
    $n = $_GET['campaign'];
    $new_bid = $_GET['new_bid'];
    $campaign = $changes[$geo]['campaigns'][$n];
    foreach ($campaign['adgroups'] as $i=>$adgroup) $changes[$geo]['campaigns'][$n]['adgroups'][$i]['actual_bid'] = $new_bid;
    file_put_contents("changes_to_be_done.arr",serialize($changes));
    header("Location: index.php?op=change&cid=$cid");
    exit;
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
?>
<script type="text/javascript">
function change_bid(geo,campaign,current_bid) {
    var new_bid = prompt("Current bid is "+current_bid+" ($"+(current_bid/1000000).toFixed(2)+")",current_bid);
    if (new_bid) location.href="index.php?op=change&cid=<?=$cid?>&newbid=1&geo="+geo+"&campaign="+campaign+"&new_bid="+new_bid;
}
</script>
<b>CID #<?=$cid?></b><br/><br/>
<?php
foreach ($changes as $geo=>$array) { ?>
<u><?=$geo?></u><br/>
<?php    
    foreach ($array['campaigns'] as $a=>$campaign) {
        if (!$campaign['adgroups']) continue;
?>
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td>campaign</td>
        <td>geo</td>
        <td>bidding</td>
        <td>adgroup_id</td>
        <td>adgroup_name</td>
        <td>status</td>
        <td>current bid</td>
        <td>new bid</td>
        <td>diff</td>
    </tr>
<?php
        foreach ($campaign['adgroups'] as $b=>$adgroup) { ?>
    <tr>
        <td><?=$campaign['campaign_name']?></td>
        <td><?=$campaign['geo_location']?></td>
        <td><?=$campaign['bidding_strategy']?></td>
        <td><?=$adgroup['adgroup_id']?></td>
        <td><?=$adgroup['adgroup_name']?></td>
        <td><?=$adgroup['status']?></td>
        <td>$<?=number_format($adgroup['bid']/1000000,2)?></td>
        <td><b>$<?=number_format($adgroup['actual_bid']/1000000,2)?></b></td>
        <td>$<?=number_format($adgroup['diff']/1000000,2)?></td>
    </tr>    
<?php   
            $actual_bid = $adgroup['actual_bid'];
        } ?>
</table>
<br/>
<button onclick="change_bid('<?=$geo?>',<?=$a?>,'<?=$actual_bid?>')">[<?=$campaign['campaign_name']?>] set different bid</button><br/><br/><br/>
<?php
    }
}
?>


<br/><br/><br/>
<button onclick="location.href='index.php?op=changebids&cid=<?=$cid?>'">Approved.  DO changes!</button>

