<?php
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$fw = 0;
$cid = $cids[$_GET['cid']];                                                     // get the CID information (client_id / name)
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function start_log() { global $fw; $fw = fopen('collect.log', 'a'); }
function do_log($message) { global $fw; fwrite($fw,"$message\r\n"); }
function close_log() { global $fw; fclose($fw); }
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// get_campaigns_locations: function that gets the locations for the selected campaigns list
function get_campaigns_locations(&$campaigns) {
    global $adwords;
    $campaign_ids = array();
    $campaigns_indexed = array();
    do_log("getting geo targetting for the campaigns..");
    foreach ($campaigns as $n=>$campaign) {                                     // move over all the campaigns..
        $campaign_ids[] = $campaign->id;                                        // add the campaign-id to a list of IDs that we will use to query the locations for them with 1 query
        $campaigns_indexed[$campaign->id] = $campaign;                          // add the campaign to an indexed array by campaign_id
    }
    $locations = $adwords->get_campaign_targetting($campaign_ids,array('Id','CriteriaType'),array('LOCATION')); // query locations for the campaign_ids list
    do_log("getting actual location data for all location ids..");
    $geos = $adwords->get_locations($locations);
    do_log("connecting locations to campaigns..");
    foreach ($locations as $n=>$location) $campaigns_indexed[$location->campaignId]->location = $geos[$location->criterion->id];
    return $campaigns_indexed;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// update_budgets: function that loads the budgets for the current client into DB
function update_adgroups($client_id) {
    global $local_db;
    $local_db->delete('adgroups',"client_id=$client_id");                         // delete the budgets table to be sure we get ACCURATE budgets
    $handle = opendir("./");
    while ($file = readdir($handle)) {                                          // ** look for ARR files **
        if($file!="." && $file!="..") {
            $arr = explode(".",strtolower($file));
            $ext = strtolower($arr[sizeof($arr)-1]);
            if ($ext=="arr") {                                                  // if file is .arr - open it and insert the data into the DB
                do_log("updating $file..");
                $local_db->startTransaction();
                $adgroups = unserialize(file_get_contents($file));              // the data in the .arr file is a serialize'd array
                foreach ($adgroups as $n=>$adgroup) {
                    $bids = explode("|",$adgroup[6]);
                    array_pop($bids);
                    list($bid,$bidtype) = explode(",",$bids[0]);
                    $local_db->insert("adgroups",array(
                            'client_id'         => $client_id,
                            'adgroup_id'        => $adgroup[0],
                            'adgroup_name'      => $adgroup[1],
                            'campaign_id'       => $adgroup[2],
                            'status'            => $adgroup[3],
                            'bidding_strategy'  => $adgroup[4],
                            'bid_type'          => $adgroup[5],
                            'bid'               => $bid
                        ));
                }
                $local_db->commit();
                unlink($file);
            }
        }
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// get_adgroups_of_campaigns: function that loads the list of campaigns from google adwords account from our local DB
function get_adgroups_of_campaigns($campaigns,$limit=false,$client_id) {
    global $adwords;
    do_log("getting adgroups of the campaigns..");
    $n=0;
    $g=10;                                                                      // get adgroups in blocks of 10 campaigns per request
    while ($n<count($campaigns)) {
        $campaigns_ids = array();
        for ($i=$n; ($i<$n+$g)&&($i<count($campaigns)); $i++) $campaigns_ids[] = $campaigns[$i]->id;
        $results = $adwords->get_adgroups($campaigns_ids,$limit);
        do_log(count($results)." adgroups");
        file_put_contents("campaign_{$campaigns[$n]->id}_adgroups.arr", serialize($results));    // need to save it into files here because on some clients the data will cause memory leak
        $n = $i;
        do_log("saved $n campaigns adgroup");
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// update_budgets: function that loads the budgets for the current client into DB
function update_campaigns($campaigns_indexed, $client_id) {
    global $local_db;
    $local_db->delete('campaigns',"client_id=$client_id");                                           // delete the budgets table to be sure we get ACCURATE budgets
    foreach ($campaigns_indexed as $campaign_id=>$campaign) {
        $local_db->insert("campaigns",array(
                'client_id'         => $client_id,
                'campaign_id'       => $campaign_id,
                'campaign_name'     => $campaign->name,
                'start_date'        => date("Y-m-d",strtotime($campaign->startDate)),
                'end_date'          => date("Y-m-d",strtotime($campaign->endDate)),
                'bidding_strategy'  => $campaign->budget->biddingStrategyConfiguration->biddingStrategyType,
                'budget_id'         => $campaign->budget->budgetId,
                'geo_location'      => (isset($campaign->location))?(($campaign->location->displayType=="State")?"{$campaign->location->parentLocations[0]->locationName} ({$campaign->location->locationName})":$campaign->location->locationName):'',
                'status'            => $campaign->status
            ));
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// update_budgets: function that loads the budgets for the current client into DB
function update_budgets($budgets, $client_id) {
    global $local_db;
    $local_db->delete('budgets',"client_id=$client_id");                        // delete the budgets table to be sure we get ACCURATE budgets
    foreach ($budgets as $budget_id=>$budget) {
        $local_db->insert("budgets",array(
            'client_id'             => $client_id,
            'budget_id'             => $budget_id,
            'budget_name'           => $budget->name,
            'period'                => $budget->period,
            'amount'                => $budget->amount->microAmount,
            'delivery_method'       => $budget->deliveryMethod,
            'reference_count'       => $budget->referenceCount,
            'is_explicitly_shared'  => ($budget->isExplicitlyShared)?1:0,
            'status'                => $budget->status,
            'bidding_strategy'      => (isset($budget->biddingStrategyConfiguration))?$budget->biddingStrategyConfiguration->biddingStrategyType:'UNKNOWN'
            ));
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// update_spends: function that updates the spendings data into the DB
function update_spends($client_id) {
    global $local_db;
    $local_db->delete('adgroups_stats',"client_id=$client_id AND date_time=%s",date("Y-m-d",time()-86400));     // delete the budgets table to be sure we get ACCURATE budgets
    $handle = opendir("./");
    while ($file = readdir($handle)) {                                          // ** look for ARR files **
        if($file!="." && $file!="..") {
            $arr = explode(".",strtolower($file));
            $ext = strtolower($arr[sizeof($arr)-1]);
            if ($ext=="arr") {                                                  // if file is .arr - open it and insert the data into the DB
                do_log("updating $file..");
                $local_db->startTransaction();
                $spends = unserialize(file_get_contents($file));                // the data in the .arr file is a serialize'd array   
                foreach ($spends as $n=>$spend) {
                    $local_db->insert("adgroups_stats",array(
                            'client_id'         => $client_id,
                            'date_time'         => $spend['DATETIME'],
                            'campaign_id'       => $spend['CAMPAIGNID'],
                            'adgroup_id'        => $spend['ADGROUPID'],
                            'keyword_id'        => $spend['KEYWORDID'],
                            'placement'         => $spend['KEYWORDPLACEMENT'],
                            'criteria_type'     => $spend['CRITERIATYPE'],
                            'impressions'       => $spend['IMPRESSIONS'],
                            'clicks'            => $spend['CLICKS'],
                            'cost'              => $spend['COST']
                        ));
                }
                $local_db->commit();
                unlink($file);
            }
        }
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// load_spends: function that loads the list of spends on all campaigns on the account
function load_spends($client_id,$campaigns) {
    global $adwords;
    $n=0;
    $g=($client_id=="6539967406")?2:50;                                         // get adgroups in blocks of 20 campaigns per request
    while ($n<count($campaigns)) {
        $campaigns_ids = array();
        for ($i=$n; ($i<$n+$g)&&($i<count($campaigns)); $i++) $campaigns_ids[] = $campaigns[$i]->id;
        $results = $adwords->get_yesterday_spends($client_id,$campaigns_ids);
        echo count($results)." spends\r\n";
        file_put_contents("campaign_{$campaigns[$n]->id}_spends.arr", serialize($results));    // need to save it into files here because on some clients the data will cause memory leak
        $n = $i;
        echo "saved $n spends\r\n";
    }
    echo "finished getting spends\r\n";
}
// -----------------------------------------------------------------------------------------------------------------------------------------------> collect adwords data for the CID
start_log();
$adwords->switch_user($cid['client_id'],false);                                 // switch the current Adwords user to our desired CID client
do_log("adwords: switched user");
$campaigns = $adwords->get_campaigns(Array( "Id",                               // get campaigns list for the client
                                            "Name",
                                            "Status",
                                            "Settings",
                                            "StartDate",
                                            "EndDate" ) );
do_log("adwords: brought campaigns");
// ================================================================================================================================================================================
load_spends($cid['client_id'],$campaigns);                                      // spends are saved into files
update_spends($cid['client_id']);                                               // update them into DB
do_log("adwords: brought spends");
// ================================================================================================================================================================================
$budgets = $adwords->get_budgets();                                             // get all the budgets of the client
do_log("adwords: brought budgets");
$campaign_budgets = $adwords->get_budgets_campaigns2($budgets);                 // get the connections between budgets & campaigns
do_log("adwords: brought budgets for campaign");
update_budgets($budgets,$cid['client_id']);                                     // update them into DB
// ================================================================================================================================================================================
$adwords->connect_campaigns_to_budgets($campaigns, $budgets, $campaign_budgets);// connect the budgets back to the campaigns array
do_log("adwords: connected campaigns to budgets");
$campaigns_indexed = get_campaigns_locations($campaigns);                       // get campaigns geo locations
do_log("adwords: brought campaigns locations");
update_campaigns($campaigns_indexed,$cid['client_id']);                         // update them into DB
// ================================================================================================================================================================================
do_log(count($campaigns)." campaigns");
get_adgroups_of_campaigns($campaigns,false,$cid['client_id']);                  // adgroups are saved into files
do_log("adwords: brought adgroups");
update_adgroups($cid['client_id']);                                             // update them into DB
// -----------------------------------------------------------------------------------------------------------------------------------------------> save the adwords info into DB
do_log("done!");
close_log();
?>