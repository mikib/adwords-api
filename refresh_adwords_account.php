<?php
// refresh_adwords_account.php - a script that should run in a cron and download & modify all the data from the adwords account and update it to the DB
// ====================================================================================================================================================================================
require_once "adwordsapi.class.php";
require_once "meekrodb.2.2.class.php";
$adwords = new adwordsapi();
$meekro = new MeekroDB("localhost", "root", "Miki123Abc", "adwords_bidder");
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// insert_clients: function that queries the google adwords server using the api and gets list of client-ids and details related to the main MCC account
function insert_clients() {
    global $adwords,$meekro;
    echo "downloading list of clients..";
    // query list of clients from the adwords account using adwords api with all fields we require
    $clients = $adwords->get_clients(array("CustomerId","Login","Name","TestAccount","CompanyName"));       
    if (count($clients)) {                                                      // if clients found..
        echo count($clients)." clients found\r\n"."inserting to DB..";          // insert them into the DB
        foreach ($clients as $n=>$client) {
            $meekro->insert("clients",array(
                'client_id'     =>  $client->customerId,
                'client_name'   =>  $client->name,
                'company_name'  =>  $client->companyName,
                'login'         =>  $client->login,
                'test_account'  =>  $client->testAccount,
                'status'        =>  1
               ));
        }
        echo "done\r\n";
    }
    return $meekro->query("SELECT * FROM clients");
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// load_clients: function that loads the list of client-ids from google adwords account from our local DB
function load_clients() {
    global $meekro;
    $results = $meekro->query("SELECT * FROM clients");                         // get adwords account clients list from DB
    if (!count($results)) insert_clients();                                     // if no results found, insert the clients into the DB    
    return $results;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// update_budgets: function that loads the budgets for the current client into DB
function update_budgets($budgets, $client_id) {
    global $meekro;
    echo "updating budgets into DB..";
    $meekro->delete('budgets',"client_id=$client_id");                                           // delete the budgets table to be sure we get ACCURATE budgets
    foreach ($budgets as $budget_id=>$budget) {
        $meekro->insert("budgets",array(
            'client_id'             => $client_id,
            'budget_id'             => $budget_id,
            'budget_name'           => $budget->name,
            'period'                => $budget->period,
            'amount'                => $budget->amount->microAmount,
            'delivery_method'       => $budget->deliveryMethod,
            'reference_count'       => $budget->referenceCount,
            'is_explicitly_shared'  => ($budget->isExplicitlyShared)?1:0,
            'status'                => $budget->status,
            'bidding_strategy'      => (isset($budget->biddingStrategyConfiguration))?$budget->biddingStrategyConfiguration->biddingStrategyType:'UNKNOWN'
            ));
    }
    echo "done\r\n";
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// update_budgets: function that loads the budgets for the current client into DB
function update_campaigns($campaigns_indexed, $client_id) {
    global $meekro;
    echo "updating campaigns into DB..";
    $meekro->delete('campaigns',"client_id=$client_id");                                           // delete the budgets table to be sure we get ACCURATE budgets
    foreach ($campaigns_indexed as $campaign_id=>$campaign) {
        $meekro->insert("campaigns",array(
                'client_id'         => $client_id,
                'campaign_id'       => $campaign_id,
                'campaign_name'     => $campaign->name,
                'start_date'        => date("Y-m-d",strtotime($campaign->startDate)),
                'end_date'          => date("Y-m-d",strtotime($campaign->endDate)),
                'bidding_strategy'  => $campaign->budget->biddingStrategyConfiguration->biddingStrategyType,
                'budget_id'         => $campaign->budget->budgetId,
                'geo_location'      => (isset($campaign->location))?(($campaign->location->displayType=="State")?"{$campaign->location->parentLocations[0]->locationName} ({$campaign->location->locationName})":$campaign->location->locationName):'',
                'status'            => $campaign->status
            ));
    }
    echo "done\r\n";
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// update_spends: function that updates the spendings data into the DB
function update_spends($client_id) {
    global $meekro;
    echo "updating spends into DB..";
    $meekro->delete('adgroups_stats',"client_id=$client_id AND date_time=%s",date("Y-m-d",time()-86400));   // delete the spends table to be sure we get ACCURATE spends
    $handle = opendir("./");
    while ($file = readdir($handle)) {                                          // ** look for ARR files **
        if($file!="." && $file!="..") {
            $arr = explode(".",strtolower($file));
            $ext = strtolower($arr[sizeof($arr)-1]);
            if ($ext=="arr") {                                                  // if file is .arr - open it and insert the data into the DB
                echo "updating $file..";
                $meekro->startTransaction();
                $spends = unserialize(file_get_contents($file));              // the data in the .arr file is a serialize'd array   
                foreach ($spends as $n=>$spend) {
                    $meekro->insert("adgroups_stats",array(
                            'client_id'         => $client_id,
                            'date_time'         => $spend['DATETIME'],
                            'campaign_id'       => $spend['CAMPAIGNID'],
                            'adgroup_id'        => $spend['ADGROUPID'],
                            'keyword_id'        => $spend['KEYWORDID'],
                            'placement'         => $spend['KEYWORDPLACEMENT'],
                            'criteria_type'     => $spend['CRITERIATYPE'],
                            'impressions'       => $spend['IMPRESSIONS'],
                            'clicks'            => $spend['CLICKS'],
                            'cost'              => $spend['COST']
                        ));
                }
                $meekro->commit();
                echo "done\r\n";
                unlink($file);
            }
        }
    }
    echo "finished\r\n";
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// update_budgets: function that loads the budgets for the current client into DB
function update_adgroups($client_id) {
    global $meekro;
    echo "updating adgroups into DB..";
    $meekro->delete('adgroups',"client_id=$client_id");                         // delete the budgets table to be sure we get ACCURATE budgets
    $handle = opendir("./");
    while ($file = readdir($handle)) {                                          // ** look for ARR files **
        if($file!="." && $file!="..") {
            $arr = explode(".",strtolower($file));
            $ext = strtolower($arr[sizeof($arr)-1]);
            if ($ext=="arr") {                                                  // if file is .arr - open it and insert the data into the DB
                echo "updating $file..";
                $meekro->startTransaction();
                $adgroups = unserialize(file_get_contents($file));              // the data in the .arr file is a serialize'd array
                foreach ($adgroups as $n=>$adgroup) {
                    $bids = explode("|",$adgroup[6]);
                    array_pop($bids);
                    if (count($bids)>1) { print_r(adgroup); die("error: number of bids>1 on "); }
                    //else list($bid,$bidtype) = explode(",",$bids[0]);
                    list($bid,$bidtype) = explode(",",$bids[0]);
                    $meekro->insert("adgroups",array(
                            'client_id'         => $client_id,
                            'adgroup_id'        => $adgroup[0],
                            'adgroup_name'      => $adgroup[1],
                            'campaign_id'       => $adgroup[2],
                            'status'            => $adgroup[3],
                            'bidding_strategy'  => $adgroup[4],
                            'bid_type'          => $adgroup[5],
                            'bid'               => $bid
                        ));
                }
                $meekro->commit();
                echo "done\r\n";
                unlink($file);
            }
        }
    }
    echo "finished\r\n";
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// connect_campaigns_budgets: function that "connects" the budgets to the related campaigns
function connect_campaigns_budgets(&$campaigns,$budgets,$campaigns_budgets) {
    echo "connecting campaigns to budgets..";
    foreach ($campaigns as $n=>$campaign) $campaigns[$n]->budget = $budgets[$campaigns_budgets[$campaign->id]];
    echo "done\r\n";
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// get_campaigns_locations: function that gets the locations for the selected campaigns list
function get_campaigns_locations(&$campaigns) {
    global $adwords;
    $campaign_ids = array();
    $campaigns_indexed = array();
    echo "getting geo targetting for the campaigns..";
    foreach ($campaigns as $n=>$campaign) {                                     // move over all the campaigns..
        $campaign_ids[] = $campaign->id;                                        // add the campaign-id to a list of IDs that we will use to query the locations for them with 1 query
        $campaigns_indexed[$campaign->id] = $campaign;                          // add the campaign to an indexed array by campaign_id
    }
    $locations = $adwords->get_campaign_targetting($campaign_ids,array('Id','CriteriaType'),array('LOCATION')); // query locations for the campaign_ids list
    echo "done\r\n"."getting actual location data for all location ids..";
    $geos = $adwords->get_locations($locations);
    echo "done\r\n"."connecting locations to campaigns..";
    foreach ($locations as $n=>$location) $campaigns_indexed[$location->campaignId]->location = $geos[$location->criterion->id];
    echo "done\r\n";
    return $campaigns_indexed;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// get_adgroups_of_campaigns: function that loads the list of campaigns from google adwords account from our local DB
function get_adgroups_of_campaigns($campaigns,$limit=false,$client_id) {
    global $adwords;
    echo "getting adgroups of the campaigns..";
    $n=0;
    $g=10;                                                                      // get adgroups in blocks of 10 campaigns per request
    while ($n<count($campaigns)) {
        $campaigns_ids = array();
        for ($i=$n; ($i<$n+$g)&&($i<count($campaigns)); $i++) $campaigns_ids[] = $campaigns[$i]->id;
        $results = $adwords->get_adgroups($campaigns_ids,$limit);
        echo count($results)." adgroups\r\n";
        file_put_contents("campaign_{$campaigns[$n]->id}_adgroups.arr", serialize($results));    // need to save it into files here because on some clients the data will cause memory leak
        $n = $i;
        echo "saved $n campaigns adgroup\r\n";
    }
    echo "finished getting adgroups\r\n";
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// delete_arr_files: function that will delete all the .arr files generated in case of an error
function delete_arr_files() {                                   
    $handle = opendir("./");
    while ($file = readdir($handle)) {                                          // ** look for ARR files **
        if($file!="." && $file!="..") {
            $arr = explode(".",strtolower($file));
            $ext = strtolower($arr[sizeof($arr)-1]);
            if ($ext=="arr") unlink($file);
        }
    }  
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// load_campaigns: function that loads the list of campaigns from google adwords account from our local DB
function load_campaigns($clients) {
    global $meekro,$adwords;
    foreach ($clients as $n=>$client) {
        $error = 1;
        while ($error) {
            try {
                $adwords->switch_user($client['client_id'],false);
                echo "getting campaigns for client '{$client['client_id']}' ({$client['client_name']})..";
                $campaigns = $adwords->get_campaigns(Array("Id","Name","Status","Settings","StartDate","EndDate"));
                echo count($campaigns)." found\r\n";
                if (!count($campaigns)) { $error=0; continue; }
                echo "getting spends for client '{$client['client_id']}' ({$client['client_name']})..";
                load_spends($client['client_id'],$campaigns);                           // spends are saved into files
                update_spends($client['client_id']);
                echo "getting all budgets list..";
                $budgets = $adwords->get_budgets();
                echo count($budgets)." budgets found\r\n";
                echo "getting budgets to campaign relations..";
                $campaigns_budgets = $adwords->get_budgets_campaigns2($budgets);
                echo count($campaigns_budgets)." connections found\r\n";
                update_budgets($budgets,$client['client_id']);
                connect_campaigns_budgets($campaigns,$budgets,$campaigns_budgets);
                $campaigns_indexed = get_campaigns_locations($campaigns);
                update_campaigns($campaigns_indexed,$client['client_id']);
                get_adgroups_of_campaigns($campaigns,false,$client['client_id']);   // adgroups are saved into files
                // save campaigns to DB
                update_adgroups($client['client_id']);
                $error=0;
            } catch (Exception $e) { 
                echo "error occured, retrying..\r\n";
                delete_arr_files();
                unset($campaigns_indexed);                                      // clean memory
                unset($campaigns_budgets);                                      // clean memory
                unset($campaigns);                                              // clean memory
                unset($budgets);                                                // clean memory
                $error=1;
            }                                        // if exception occured - retry
        }
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// load_spends: function that loads the list of spends on all campaigns on the account
function load_spends($client_id,$campaigns) {
    global $meekro,$adwords;
    $n=0;
    $g=($client_id=="6539967406")?2:50;                                         // get adgroups in blocks of 20 campaigns per request
    while ($n<count($campaigns)) {
        $campaigns_ids = array();
        for ($i=$n; ($i<$n+$g)&&($i<count($campaigns)); $i++) $campaigns_ids[] = $campaigns[$i]->id;
        $results = $adwords->get_yesterday_spends($client_id,$campaigns_ids);
        echo count($results)." spends\r\n";
        file_put_contents("campaign_{$campaigns[$n]->id}_spends.arr", serialize($results));    // need to save it into files here because on some clients the data will cause memory leak
        $n = $i;
        echo "saved $n spends\r\n";
    }
    echo "finished getting spends\r\n";
}
// ====================================================================================================================================================================================
// ====================================================================================================================================================================================
// ====================================================================================================================================================================================
// ====================================================================================================================================================================================
//$spends = load_spends();
delete_arr_files();
$clients = load_clients();                                                      // <-- start program: load clients list
$campaigns = load_campaigns($clients);                                          // <-- load list of campaigns associated with clients
?>