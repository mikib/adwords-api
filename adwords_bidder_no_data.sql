-- --------------------------------------------------------
-- מארח:                         127.0.0.1
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL גירסא:               8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for adwords_bidder
CREATE DATABASE IF NOT EXISTS `adwords_bidder` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `adwords_bidder`;


-- Dumping structure for table adwords_bidder.adgroups
CREATE TABLE IF NOT EXISTS `adgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL DEFAULT '0',
  `adgroup_id` bigint(20) NOT NULL DEFAULT '0',
  `adgroup_name` varchar(200) NOT NULL DEFAULT '0',
  `campaign_id` bigint(20) NOT NULL DEFAULT '0',
  `status` enum('ENABLED','PAUSED','DELETED') NOT NULL DEFAULT 'ENABLED',
  `bidding_strategy` enum('BUDGET_OPTIMIZER','CONVERSION_OPTIMIZER','MANUAL_CPC','MANUAL_CPM','PAGE_ONE_PROMOTED','PERCENT_CPA','TARGET_SPEND','ENHANCED_CPC','TARGET_CPA','TARGET_ROAS','NONE','UNKNOWN') NOT NULL DEFAULT 'UNKNOWN',
  `bid_type` enum('MANUAL_CPM','MANUAL_CPC','TARGET_CPA') NOT NULL DEFAULT 'TARGET_CPA',
  `bid` bigint(20) NOT NULL,
  `number_bids` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  KEY `adgroup_id` (`adgroup_id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table adwords_bidder.adgroups_stats
CREATE TABLE IF NOT EXISTS `adgroups_stats` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL DEFAULT '0',
  `date_time` date NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `adgroup_id` bigint(20) NOT NULL,
  `keyword_id` bigint(20) NOT NULL,
  `placement` varchar(200) NOT NULL,
  `criteria_type` varchar(50) NOT NULL,
  `impressions` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `cost` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  KEY `impressions` (`impressions`),
  KEY `clicks` (`clicks`),
  KEY `cost` (`cost`),
  KEY `campaign_id` (`campaign_id`),
  KEY `adgroup_id` (`adgroup_id`),
  KEY `keyword_id` (`keyword_id`),
  KEY `date_time` (`date_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table adwords_bidder.budgets
CREATE TABLE IF NOT EXISTS `budgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL DEFAULT '0',
  `budget_id` bigint(20) NOT NULL DEFAULT '0',
  `budget_name` varchar(100) NOT NULL DEFAULT '0',
  `period` enum('DAILY','MONTHLY') NOT NULL DEFAULT 'DAILY',
  `amount` bigint(20) NOT NULL DEFAULT '0',
  `delivery_method` enum('STANDARD','ACCELERATED') NOT NULL DEFAULT 'STANDARD',
  `reference_count` int(11) NOT NULL DEFAULT '0',
  `is_explicitly_shared` int(11) NOT NULL DEFAULT '0',
  `status` enum('ACTIVE','DELETED','UNKNOWN') NOT NULL DEFAULT 'UNKNOWN',
  `bidding_strategy` enum('BUDGET_OPTIMIZER','CONVERSION_OPTIMIZER','MANUAL_CPC','MANUAL_CPM','PAGE_ONE_PROMOTED','PERCENT_CPA','TARGET_SPEND','ENHANCED_CPC','TARGET_CPA','TARGET_ROAS','NONE','UNKNOWN') NOT NULL DEFAULT 'UNKNOWN',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table adwords_bidder.campaigns
CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL DEFAULT '0',
  `campaign_id` bigint(20) NOT NULL DEFAULT '0',
  `campaign_name` varchar(80) NOT NULL DEFAULT '0',
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `bidding_strategy` enum('BUDGET_OPTIMIZER','CONVERSION_OPTIMIZER','MANUAL_CPC','MANUAL_CPM','PAGE_ONE_PROMOTED','PERCENT_CPA','TARGET_SPEND','ENHANCED_CPC','TARGET_CPA','TARGET_ROAS','NONE','UNKNOWN') NOT NULL DEFAULT 'UNKNOWN',
  `budget_id` bigint(20) NOT NULL,
  `geo_location` varchar(30) NOT NULL,
  `status` enum('ACTIVE','PAUSED','DELETED') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table adwords_bidder.clients
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL,
  `client_name` varchar(80) NOT NULL,
  `company_name` varchar(80) NOT NULL,
  `login` varchar(80) NOT NULL,
  `test_account` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table adwords_bidder.reporting_data
CREATE TABLE IF NOT EXISTS `reporting_data` (
  `cid` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `revenue` float NOT NULL DEFAULT '0',
  `spend` float NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `pixels` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
