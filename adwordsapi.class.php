<?php
// Google Adwords API PHP class v1.0
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
require_once dirname(__FILE__) . "/init.php";                                   // this is required to allow all of Google's Adwords API functionality available in our class
// ====================================================================================================================================================================================
class adwordsapi {
    const   ADWORDS_PAGE_SIZE = AdWordsConstants::RECOMMENDED_PAGE_SIZE;        // adwords_page_size = the recommended page size for google adwords api
    var     $user = null;                                                       // user: contains the AdwordsUser class
    var     $client_id = null;                                                  // client_id: contains the CID (in case we queried it)
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // adwordsapi: class constructor and google AdwordsUser initialization.  To query the entire MCC account, leave client_id NULL.  If we want specific client, use client_id
    function adwordsapi($client_id=NULL, $log=true) {
        if ($client_id) {                                                       // if client_id is passed - create AdWordsUser for client_id
            $this->user = new AdWordsUser(NULL, NULL, NULL, NULL, NULL, NULL, $client_id);
            $this->client_id = $client_id;
        } else $this->user = new AdWordsUser();                                 // if not - create AdWordsUser for the entire account
        if ($log) $this->user->LogAll();
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // logging: function that enables logging 
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // switch_user: function that switches the AdWordsUser class, useful when access specific client's information
    function switch_user($client_id, $log=true) {
        if ($this->user) {
            unset($this->user);
            $this->user = new AdWordsUser(NULL, NULL, NULL, NULL, NULL, NULL, $client_id);
            if ($log) $this->user->LogAll();
        }
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // create_service: helper function, performs the GetService function to create the desired service by $name
    function get_service($name) {
        if (!isset($this->user)) throw new Exception('undeclared AdWordsUser'); // safe check: make sure we have defined the user
        return $this->user->GetService($name, ADWORDS_VERSION);                 // get the desired service from google adwords api
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_clients: get list of all clients (CIDs) associated with the current user
    function get_clients($fields=Array("CustomerId","Name"), $order=Array("Name","ASCENDING")) {
        $output = array();                                                      // define the output array in case we didn't define it yet
        $service = $this->get_service('ManagedCustomerService');                // <-- define the service we query (ManagedCustomerService)
        $selector = new Selector();                                             // <-- define the selector, a query object for adwords api    
        $selector->fields = $fields;                                            // define the chosen fields for our selector
        $selector->ordering[] = new OrderBy($order[0],$order[1]);               // define the ordering, pass the array's members to the orderby constructor as arguments
        $selector->paging = new Paging(0, $this::ADWORDS_PAGE_SIZE);            // define paging for the selector, specifically the page size to query (use default)
        // start looping through the results (if found)
        do {
            $page = $service->get($selector);                                   // perform the get request to google servers
            if (isset($page->entries)) foreach ($page->entries as $result) if (strlen($result->name)) $output[]=$result;   // if results are found.. and bring only clients with name
            $selector->paging->startIndex += $this::ADWORDS_PAGE_SIZE;          // increase the start index for the next query by default page size
        } while ($page->totalNumEntries > $selector->paging->startIndex);       // <-- loop until we got all results
        return $output;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_campaigns: get list of all campaigns that belong to the current user
    function get_campaigns($fields=Array("Id","Name","BiddingStrategyType","BiddingStrategyName","Status"), $order=Array("Name","ASCENDING")) {
        $output = array();                                                      // define the output array in case we didn't define it yet
        $service = $this->get_service("CampaignService");                       // <-- define the service we query (CampaignService)
        $selector = new Selector();
        $selector->fields = $fields;
        $selector->ordering[] = new OrderBy($order[0],$order[1]);
        $selector->paging = new Paging(0, $this::ADWORDS_PAGE_SIZE);
        do {
            $page = $service->get($selector);
            if (isset($page->entries)) foreach ($page->entries as $result) $output[] = $result;
            $selector->paging->startIndex += $this::ADWORDS_PAGE_SIZE;          // increase the start index for the next query by default page size
        } while ($page->totalNumEntries > $selector->paging->startIndex);       // <-- loop until we got all results
        return $output;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_location: function that takes in a location ID (number), and returns the geographical location on the globe
    function get_location($location_id, $fields=array('Id','LocationName','CanonicalName','DisplayType','ParentLocations','Reach','TargetingStatus')) {
        $output = array();
        $service = $this->get_service("LocationCriterionService");
        $selector = new Selector();
        $selector->fields = $fields;
        $selector->predicates[] = new Predicate('Id', 'IN', $location_id);      // predicate = google adwords api's filtering object
        $location = $service->get($selector);
        return $location;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_locations: function that takes in a location ID (number), and returns the geographical location on the globe
    function get_locations($locations, $fields=array('Id','LocationName','CanonicalName','DisplayType','ParentLocations','Reach','TargetingStatus')) {
        $output = array();
        $location_ids = array();
        $location_keys = array();
        foreach ($locations as $n=>$location) $location_keys[$location->criterion->id] = 1; // create a list of IDs - this is done to prevent duplicate locations
        foreach ($location_keys as $key=>$v) $location_ids[] = $key;                        // create a list of IDs of all the locations to query them in 1 time from google servers
        $service = $this->get_service("LocationCriterionService");
        $selector = new Selector();
        $selector->fields = $fields;
        $selector->predicates[] = new Predicate('Id', 'IN', $location_ids);     // predicate = google adwords api's filtering object
        $selector->paging = new Paging(0, $this::ADWORDS_PAGE_SIZE);            // define paging for the selector, specifically the page size to query (use default)
        $page = $service->get($selector);
        foreach ($page as $n=>$location) $output[$location->location->id] = $location->location;
        return $output;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_campaign_targetting: function that gets the targetting data for a specific campaign
    function get_campaign_targetting($campaign_id, $fields=array('Id','CriteriaType'), $criteria=array('LANGUAGE','LOCATION','AGE_RANGE','CARRIER','OPERATING_SYSTEM_VERSION','GENDER','POLYGON','PROXIMITY','PLATFORM'), $get_location=true) {
        $output = array();
        $service = $this->get_service("CampaignCriterionService");              // use the service called CampaignCriterionService for getting the targetting data
        $selector = new Selector();
        $selector->fields = $fields;
        // now we'll define predicates.  predicate is a special object used to create a filter for results, like a query.  we tell google what data exactly we want
        $selector->predicates[] = new Predicate('CampaignId','IN',$campaign_id);
        $selector->predicates[] = new Predicate('CriteriaType','IN',$criteria);
        $selector->paging = new Paging(0, $this::ADWORDS_PAGE_SIZE);            // define paging for the selector, specifically the page size to query (use default)
        // start looping through the results (if found)
        do {
            $page = $service->get($selector);                                   // perform the get request to google servers
            if (isset($page->entries)) foreach ($page->entries as $result) $output[] = $result;     // if results are found..
            $selector->paging->startIndex += $this::ADWORDS_PAGE_SIZE;          // increase the start index for the next query by default page size
        } while ($page->totalNumEntries > $selector->paging->startIndex);       // <-- loop until we got all results
        return $output;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_adgroups: function that gets an array of adgroups that are related to a specific campaign ID
    function get_adgroups($campaign_ids,$limit=false) {
        $output = array();
        $service = $this->get_service("AdGroupService");
        $selector = new Selector();
        $selector->fields = array('BidType','BiddingStrategyId','BiddingStrategyType','CampaignId','CpcBid','CpmBid','ContentBid','TargetCpaBid','Status','Name');        
        $selector->predicates[] = new Predicate('CampaignId','IN',$campaign_ids);
        $selector->paging = new Paging(0, $this::ADWORDS_PAGE_SIZE);
        do {
            $page = $service->get($selector);
            if (isset($page->entries)) {
                foreach ($page->entries as $result) {
                    $bids = "";
                    //if (!isset($result->biddingStrategyConfiguration->bids)) { print_r($result); exit; }
                    //if ($result->id=="4640229942") print_r($result);
                    if (count($result->biddingStrategyConfiguration->bids)>1) { // if there are more than 1 bid - choose the bid that is relevant to the bidding strategy (cpm,cpc,cpa)
                        foreach ($result->biddingStrategyConfiguration->bids as $n=>$bid) {
                            switch ($result->biddingStrategyConfiguration->biddingStrategyType) {
                                case "MANUAL_CPC": if ($bid->BidsType=="CpcBid") $bids .= "{$bid->bid->microAmount},{$bid->BidsType}|"; break;
                                case "MANUAL_CPM": if ($bid->BidsType=="CpmBid") $bids .= "{$bid->bid->microAmount},{$bid->BidsType}|"; break;
                                case "CONVERSION_OPTIMIZER": if ($bid->BidsType=="CpaBid") $bids .= "{$bid->bid->microAmount},{$bid->BidsType}|"; break;
                            }
                        }
                    } else foreach ($result->biddingStrategyConfiguration->bids as $n=>$bid) $bids .= "{$bid->bid->microAmount},{$bid->BidsType}|";
                    $result_arr = array($result->id,
                                        $result->name,
                                        $result->campaignId,
                                        $result->status,
                                        $result->biddingStrategyConfiguration->biddingStrategyType,
                                        (is_object($result->biddingStrategyConfiguration->biddingScheme))?$result->biddingStrategyConfiguration->biddingScheme->bidType:'MANUAL_CPC',
                                        $bids
                                    );
                    //if ($result_arr[6]!='TARGET_CPA') print_r($result_arr);     // for debugging
                    $output[] = $result_arr;
                }
                echo count($output)."\r\n";
                if ($limit) if (count($output)>=$limit) return $output;         // $limit allows us to limit the number of adgroups we want to return
            }
            $selector->paging->startIndex += $this::ADWORDS_PAGE_SIZE;          // increase the start index for the next query by default page size
        } while ($page->totalNumEntries > $selector->paging->startIndex);       // <-- loop until we got all results
        unset($selector->paging);
        unset($selector->predicates);
        unset($selector->fields);
        unset($selector);
        unset($service);
        return $output;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_spends: function that gets all the spends on the campaigns on an account from date to date
    function get_spends($from_date=null, $to_date=null, $client_id, $campaigns=null) {
        $output = array();       
        $this->user->SetClientId($client_id);
        $this->user->LoadService('ReportDefinitionService', 'v201402');
        // Create selector.
        $selector = new Selector();
        $selector->fields = array('CampaignId', 'AdGroupId', 'Id', 'Criteria', 'CriteriaType', 'Impressions', 'Clicks', 'Cost');
        $selector->dateRange = new DateRange();                                                       // CUSTOM DATE
        $selector->dateRange->min = ($from_date)?date("Ymd",$from_date):date("Ymd",time()-86400);     // CUSTOM DATE
        $selector->dateRange->max = ($to_date)?date("Ymd",$to_date):date("Ymd",time());               // CUSTOM DATE
        // Filter out deleted criteria.
        $selector->predicates[] = new Predicate('Status', 'NOT_IN', array('DELETED'));
        if ($campaigns) $selector->predicates[] = new Predicate('CampaignId','IN',$campaigns);
        // Create report definition.
        $reportDefinition = new ReportDefinition();
        $reportDefinition->selector = $selector;
        $reportDefinition->reportName = 'Criteria performance report #' . uniqid();
        $reportDefinition->dateRangeType = 'CUSTOM_DATE';                                             // CUSTOM DATE
        $reportDefinition->reportType = 'CRITERIA_PERFORMANCE_REPORT';
        $reportDefinition->downloadFormat = 'XML';
        // Exclude criteria that haven't recieved any impressions over the date range.
        $reportDefinition->includeZeroImpressions = FALSE;
        // Set additional options.
        $options = array('version' => 'v201402', 'returnMoneyInMicros' => TRUE);
        // Download report.
        $xml = ReportUtils::DownloadReport($reportDefinition, NULL, $this->user, $options);
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xml, $vals, $index);
        xml_parser_free($p);
        //print_r($vals);                                                         // vals = the data itself
        //print_r($index);                                                        // index = some index that I dont fully understand (ask Google?)
        foreach ($vals as $n=>$a) {
            switch ($a['tag']) {
                case "DATE-RANGE":
                    $report_date = date("Y-m-d",strtotime($a['attributes']['DATE']));
                    break;
                case "ROW":
                    $o = $a['attributes'];                                      // copy the data into array "o"
                    $o['DATETIME'] = $report_date;                              // add the date time into it
                    $output[] = $o;                                             // add it to our output
                    break;
            }
        }
        return $output;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_spends: function that gets all the spends on the campaigns on an account from date to date
    function get_yesterday_spends($client_id,$campaigns=null) {
        $output = array();       
        $this->user->SetClientId($client_id);
        $this->user->LoadService('ReportDefinitionService', 'v201402');
        // Create selector.
        $selector = new Selector();
        $selector->fields = array('CampaignId', 'AdGroupId', 'Id', 'Criteria', 'CriteriaType', 'Impressions', 'Clicks', 'Cost');
        // Filter out deleted criteria.
        $selector->predicates[] = new Predicate('Status', 'NOT_IN', array('DELETED'));
        if ($campaigns) $selector->predicates[] = new Predicate('CampaignId','IN',$campaigns);
        // Create report definition.
        $reportDefinition = new ReportDefinition();
        $reportDefinition->selector = $selector;
        $reportDefinition->reportName = 'Criteria performance report #' . uniqid();
        $reportDefinition->dateRangeType = 'YESTERDAY';
        $reportDefinition->reportType = 'CRITERIA_PERFORMANCE_REPORT';
        $reportDefinition->downloadFormat = 'XML';
        // Exclude criteria that haven't recieved any impressions over the date range.
        $reportDefinition->includeZeroImpressions = FALSE;
        // Set additional options.
        $options = array('version' => 'v201402', 'returnMoneyInMicros' => TRUE);
        // Download report.
        $xml = ReportUtils::DownloadReport($reportDefinition, NULL, $this->user, $options);
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xml, $vals, $index);
        xml_parser_free($p);
        //print_r($vals);                                                         // vals = the data itself
        //print_r($index);                                                        // index = some index that I dont fully understand (ask Google?)
        foreach ($vals as $n=>$a) {
            switch ($a['tag']) {
                case "DATE-RANGE":
                    $report_date = date("Y-m-d",strtotime($a['attributes']['DATE']));
                    break;
                case "ROW":
                    $o = $a['attributes'];                                      // copy the data into array "o"
                    $o['DATETIME'] = $report_date;                              // add the date time into it
                    $output[] = $o;                                             // add it to our output
                    break;
            }
        }
        return $output;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_budgets: function that gets all the shared & individual budgets associated with the current AdwordsUser
    function get_budgets() {
        $output = array();
        $service = $this->get_service("BudgetService");
        $selector = new Selector();
        $selector->fields = array('BudgetId','Amount','BudgetName','BudgetReferenceCount','BudgetStatus','DeliveryMethod','IsBudgetExplicitlyShared','Period');        
        $selector->paging = new Paging(0, $this::ADWORDS_PAGE_SIZE);
        do {
            $page = $service->get($selector);
            if (isset($page->entries)) foreach ($page->entries as $result) {
                $output[$result->budgetId] = $result;                           // <-- here the array is indexed - the key holds the budgetId, this is better for campaigns connections
            }
            $selector->paging->startIndex += $this::ADWORDS_PAGE_SIZE;          // increase the start index for the next query by default page size
        } while ($page->totalNumEntries > $selector->paging->startIndex);       // <-- loop until we got all results
        return $output;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_budgets_campaigns: function that gets an array of budgets and finds the related campaigns for them in the current AdwordsUser
    function get_budgets_campaigns($budgets) {
        $output = array();
        $service = $this->get_service("CampaignService");
        foreach ($budgets as $key=>$budget) {
            echo "getting campaigns for budget $key..";
            $selector = new Selector();
            $selector->fields = array('Id');
            $selector->predicates[] = new Predicate('BudgetId','EQUALS',$key);
            $selector->paging = new Paging(0, $this::ADWORDS_PAGE_SIZE);
            do {
                $page = $service->get($selector);
                if (isset($page->entries)) foreach ($page->entries as $result) $output[$result->id] = $key;
                $selector->paging->startIndex += $this::ADWORDS_PAGE_SIZE;          // increase the start index for the next query by default page size
            } while ($page->totalNumEntries > $selector->paging->startIndex);       // <-- loop until we got all results
            print_r($output);
            unset($selector);
        }
        return $output;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_budgets_campaigns: function that gets an array of budgets and finds the related campaigns for them in the current AdwordsUser
    function get_budgets_campaigns2($budgets) {
        $i = 0;
        $output = array();
        $budget_ids = array();
        foreach ($budgets as $key=>$budget) $budget_ids[] = $key;
        $service = $this->get_service("CampaignService");
        $selector = new Selector();
        $selector->fields = array('Id','BiddingStrategyType');
        $selector->predicates[] = new Predicate('BudgetId','IN',$budget_ids);   // bring campaigns that their budgetId is in the list of budgets
        $selector->paging = new Paging(0, $this::ADWORDS_PAGE_SIZE);
        do {
            $page = $service->get($selector);
            if (isset($page->entries)) foreach ($page->entries as $result) {
                $key = $budget_ids[$i];
                $budgets[$key]->biddingStrategyConfiguration = $result->biddingStrategyConfiguration;   // store the bid strategy in the budget so we know
                $output[$result->id] = $budget_ids[$i++];                       // the returned list is in the same order of $budget_ids list, so we store the campaignId with budgetId
            }
            $selector->paging->startIndex += $this::ADWORDS_PAGE_SIZE;          // increase the start index for the next query by default page size
        } while ($page->totalNumEntries > $selector->paging->startIndex);       // <-- loop until we got all results
        return $output;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // connect_campaigns_budgets: function that "connects" the budgets to the related campaigns
    function connect_campaigns_to_budgets(&$campaigns,$budgets,$campaigns_budgets) {
        foreach ($campaigns as $n=>$campaign) $campaigns[$n]->budget = $budgets[$campaigns_budgets[$campaign->id]];
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // get_campaigns_for_all_clients: function that brings all clients (CIDs) and all campaigns for each client.  This function takes long to execute and should output its progress..
    function get_campaigns_for_all_clients($fields=Array("Id","Name"), $order=Array("Name","ASCENDING"), $include_targetting=true) {
        $output = array();
        echo "getting all clients..";
        $clients = $this->get_clients();                                        // get list of all clients in the main mcc account
        print_r($clients);
        foreach ($clients as $i=>$client) {                                     // move through all of them..
            $cid = $client->customerId;
            if (!strlen($client->name)) continue;                               // if name is empty - that is the main mcc account, skip it
            $this->switch_user($cid);                                           // switch the AdWordsUser class for the CID, or else we'll get no campaigns
            echo "getting campaigns for client $cid..";
            $campaigns = $this->get_campaigns(Array("Id","Name","Status","Settings"));  // get the campaigns in this cid..
            print_r($campaigns);
            if ($include_targetting) {                                          // if we want to get the targetting per each campaign
                foreach ($campaigns as $c=>$campaign) {
                    $campaigns[$c]->targetting = $this->get_campaign_targetting($campaign->id);
                    print_r($campaigns[$c]);
                }
            }
            $output[$cid] = $campaigns;                                         // add to output 
        }
        return $output;                                                         // return output
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // update_adgroups_bids: function that updates the bids on adgroups
    function update_adgroups($adgroups=array(array("adgroup_id"=>0,"bid"=>"cpa","amount"=>0))) {
        $adGroupService = $this->get_service('AdGroupService');
        $operations = array();
        if (!$adgroups[0]['adgroup_id']) return;
        foreach ($adgroups as $i=>$adgroup) {
            $ad = new AdGroup();
            $ad->id = $adgroup['adgroup_id'];
            switch ($adgroup['bid']) {
                case "cpa": $bid = new CpaBid(); break;
                case "cpc": $bid = new CpcBid(); break;
                case "cpm": $bid = new CpmBid(); break;
            }
            $amount = intval($adgroup['amount'] * AdWordsConstants::MICROS_PER_DOLLAR);         // need to eliminate any fractional digits
            $amount = round($amount, -4);                                       // the bid must be a whole multiply of 0.01$
            $bid->bid = new Money( $amount );
            $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
            $biddingStrategyConfiguration->bids[] = $bid;
            $ad->biddingStrategyConfiguration = $biddingStrategyConfiguration;
            $operation = new AdGroupOperation();
            $operation->operand = $ad;
            $operation->operator = 'SET';
            $operations[] = $operation;
        }
        //print_r($operations);
        // Make the mutate request.
        $result = $adGroupService->mutate($operations);
        $adgroups = $result->value;
    }
}
?>