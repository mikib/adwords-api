<?php
/**
 * This example sets a Bid Modifier on a Campaign.
 *
 * Tags: CampaignCriterionService.mutate
 * Restriction: adwords-only
 *
 * Copyright 2013, Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    GoogleApiAdsAdWords
 * @subpackage v201402
 * @category   WebServices
 * @copyright  2013, Google Inc. All Rights Reserved.
 * @license    http://www.apache.org/licenses/LICENSE-2.0 Apache License,
 *             Version 2.0
 * @author     Paul Matthews
 */

// Local MySQL "root" password = Miki123Abc
// https://developers.google.com/adwords/api/docs/appendix/selectorfields#v201402-BudgetService
// Include the initialization file.
require_once "adwordsapi.class.php";

try {
  // Get AdWordsUser from credentials in "../auth.ini"
  // relative to the AdWordsUser.php file's directory.
  $adwords = new adwordsapi();      // see similar test
  $clients = $adwords->get_clients();
  print_r($clients);
  exit;
  // 168670702 = "4146_Clothing Exact -US"
  //$data = $adwords->get_campaign_targetting("168670702");
  $budgets = $adwords->get_budgets();
  $campaigns = $adwords->get_campaigns();
  $adgroups = $adwords->get_adgroups("166149622");
  print_r($adgroups);
  exit;
  $campaigns_budgets = $adwords->get_budgets_campaigns2($budgets);
  echo count($campaigns);
  exit;
  print_r($campaigns);
  exit;
  $data = $adwords->get_campaigns(array("Id","Name","Status"));
  print_r($data);
  exit;
    
    
  //$adwords = new adwordsapi()
  //$results = $adwords->get_campaigns_for_all_clients();
  print_r($results);
  
} catch (Exception $e) {
  printf("An error has occurred: %s\n", $e->getMessage());
}
