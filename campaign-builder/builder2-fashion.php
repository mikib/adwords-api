<?php
ob_start("ob_gzhandler");
set_time_limit(900);
$file = "Fashion KW.csv";
$generic_words = array(
                        "adams",
                        "adidas",
                        "vittadini",
                        "aeropostal",
                        "affliction",
                        "air jordan",
                        "air max",
                        "dickies",
                        "fossil",
                        "gucci",
                        "guess",
                        "hair bow",
                        "handbags",
                        "handbag",
                        "kate spade",
                        "kipling",
                        "lesportsac",
                        "louis vuitton",
                        "lucky brand",
                        "ecko",
                        "oakely",
                        "nike",
                        "prada",
                        "tignanello",
                        "titleist",
                        "vera bradley",
                        "volcom",
                        "wayfair",
                        "dkny",
                        "ecko",
                        "bag",
                        "bags"
                    );
$generic_words = array_flip($generic_words);
/*
 *  REMEMBER TO FIX THE DISPLAY URL - REMOVE & AND ' AND OTHER BAD CHARACTERS
 */
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function    english_name($name) {                                               // english_name = helper function that converts a column name to a php kind of column name
    $output = "";
    $temp_name = strtolower($name);                                             // make the entire string lower case
    for ($i=0; $i<strlen($temp_name); $i++) {                                   // move over all the characters of the column name
        $ch = $temp_name[$i];                                                   // take the current char
        if ((($ch>='a')&&($ch<='z'))||(($ch>='0')&&($ch<='9'))) {               // allow only a-z and 0-9 characters
            $output .= $ch;
        } else if ($ch==' ') {                                                  // and convert spaces to _
            $output .= "_";
        }
    }
    return $output;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function find_generic_word($keywords,$debug=false) {
    global $generic_words, $oword;
    foreach ($keywords as $i=>$word) {
        $lword = trim(strtolower($word));
        if (isset($generic_words[$lword])) {                                    // if one of the words is found in "generic" table - use it
            $oword = $word;
            $oword[0] = strtoupper($oword[0]);
            return $oword;
        }
    }
    return "Clothing";                                                           // generic word in case no other word was found
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_virtual_ad1($keyword) {
    $words = explode(" ",$keyword);                                             // take keyword/s
    foreach ($words as $c=>$word) $words[$c][0] = strtoupper($word[0]);         // make first word in each keyword UPCASE
    $generic_word = find_generic_word($words);                                  // see if one of our generic words is in the keyword term, if it is - use it
    $words_keyword = implode(" ",$words);
    $words_display = implode("-",$words);
    $words_query = implode("%20",$words);
    // ========================================================================
    $textAd = new stdClass();
    $headline = "Wanted: $words_keyword";                       if (strlen($headline)>25) $headline = "Wanted: $generic_word Deals";
    $textAd->headline = $headline;                                              // 25 chars
    $textAd->description1 = '"I Have Been Searching for a Better';              // 35 chars
    $desc2 = 'Price for '.$words_keyword.'. Found It!"';        if (strlen($desc2)>35) $desc2 = 'Price for '.$generic_word.'. Found It!"';
    $textAd->description2 = $desc2;                                            // 35 chars
    $url = "$words_display.FindSimilar.com";                    if (strlen($url)>35) $url = "$generic_word.FindSimilar.com";
    $url = str_replace(" ","-",$url);
    $textAd->displayUrl = $url;                                                // 35 chars
    $textAd->url = "http://www.findsimilar.com/search.php?query=$words_query&cid=4778";
    // Create ad group ad.
    return $textAd;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_virtual_ad2($keyword) {
    $words = explode(" ",$keyword);                                             // take keyword/s
    foreach ($words as $c=>$word) $words[$c][0] = strtoupper($word[0]);         // make first word in each keyword UPCASE
    $generic_word = find_generic_word($words);                                  // see if one of our generic words is in the keyword term, if it is - use it
    $words_keyword = implode(" ",$words);
    $words_display = implode("-",$words);
    $words_query = implode("%20",$words);
    // ========================================================================
    $textAd = new stdClass();
    $headline = "$words_keyword- Outlet";           if (strlen($headline)>25) $headline = $words_keyword;           if (strlen($headline)>25) $headline = "$generic_word- Outlet";
    $textAd->headline = $headline;                                              // 25 chars
    $desc1 = '"I Just Bought '.$words_keyword;      if (strlen($desc1)>35) $desc1 = '"I Bought '.$words_keyword;    if (strlen($desc1)>35) $desc1 = '"I Just Bought New '.$generic_word;
    $textAd->description1 = $desc1;
    $textAd->description2 = '& Paid Less Than You Are Going To"';               // 35 chars
    $url = "$words_display.FindSimilar.com";        if (strlen($url)>35) $url = "$generic_word.FindSimilar.com";
    $url = str_replace(" ","-",$url);
    $textAd->displayUrl = $url;                                                // 35 chars
    $textAd->url = "http://www.findsimilar.com/search.php?query=$words_query&cid=4778";
    // Create ad group ad.
    return $textAd;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_virtual_ad3($keyword) {
    $flag = false;
    $tkeyword = ($keyword[strlen($keyword)-1]=="s")?$keyword:"{$keyword}s";
    $words = explode(" ",$tkeyword);                                            // take keyword/s
    foreach ($words as $c=>$word) $words[$c][0] = strtoupper($word[0]);         // make first word in each keyword UPCASE
    $generic_word = find_generic_word($words);                                  // see if one of our generic words is in the keyword term, if it is - use it
    $words_keyword = implode(" ",$words);
    $words_display = implode("-",$words);
    $words_query = implode("%20",$words);
    // ========================================================================
    $textAd = new stdClass();
    $headline = "Explore 100+ ".$words_keyword;
    if (strlen($headline)>25) {
        $headline = "Explore 100+ ".$generic_word;
        if ($headline[strlen($headline)-1]!="s") $headline .= "s";
    }
    if (strlen($headline)>25) { $flag=true; $headline = "Explore 100+ Sales"; }
    $textAd->headline = $headline;                                              // 25 chars
    $textAd->description1 = "Yes, There is Always a Better Deal";
    if ($flag) $desc2 = "for Clothing -Take a Look";
    else {
        $desc2 = "for $words_keyword -Take a Look";     
        if (strlen($desc2)>35) $desc2 = "for this -Take a Look";
    }
    $textAd->description2 = $desc2;                                             // 35 chars
    $url = "$words_display.FindSimilar.com";        if (strlen($url)>35) $url = "$generic_word.FindSimilar.com";
    $url = str_replace(" ","-",$url);
    $textAd->displayUrl = $url;                                                 // 35 chars
    $textAd->url = "http://www.findsimilar.com/search.php?query=$words_query&cid=4778";
    // Create ad group ad.
    return $textAd;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function add_virtual_ad($data=null) {
    $ad1 = create_virtual_ad1($data['keyword']);
    $ad2 = create_virtual_ad2($data['keyword']);
    $ad3 = create_virtual_ad3($data['keyword']);
    return array($ad1,$ad2,$ad3);
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ====================================================================================================================================================================================
$csv_data = file_get_contents($file);                                           // part 1 - open Michal's CSV file with keywords
$kws = explode("\n",$csv_data);
foreach ($kws as $i=>$kw) $kws[$i] = trim(str_replace(array("\r"),"",$kw));

if (isset($_GET['export'])) {
    if ($_GET['export']=="csv") {
        header('Content-Type: text/csv' );
        header('Content-Disposition: attachment; filename=fashion.csv');
        echo "Campaign,Ad Group,Keyword,Headline,Description Line 1,Description Line 2,Display URL,Destination URL\r\n";
        foreach ($kws as $i=>$kw) {
            if (strlen(trim($kw))<1) continue;
            $adgroup_name = $kw;
            $keyword = $kw;
            $cpc = "0.20";                                                              // changed manually by Miki to fit Michal's settings
            $arr = add_virtual_ad(array("keyword"=>$keyword));
            $output = array();
            foreach ($arr as $n=>$ad) {
                echo "4778_Furniture Exact -UK,$keyword,$keyword,\"{$ad->headline}\",\"".str_replace('"','""',$ad->description1)."\",\"".str_replace('"','""',$ad->description2)."\",{$ad->displayUrl},{$ad->url}\r\n";
            }
        }
        exit;
    }
}
?>
<style>
body {
    font-family:arial;
    font-size:12px;
}
</style>
<script type="text/javascript">
var ads = new Array();
<?php
foreach ($kws as $i=>$kw) {
    // NOW NEED TO PERFORM A BULK OPERATION
    if (strlen(trim($kw))<1) continue;
    $adgroup_name = $kw;
    $keyword = $kw;
    $citerion = "stam";
    $cpc = "0.20";                                                              // changed manually by Miki to fit Michal's settings
    $arr = add_virtual_ad(array("keyword"=>$keyword));
    $output = array();
    foreach ($arr as $n=>$ad) {
        $json = array(  "headline"  =>  $ad->headline,
                        "desc1"     =>  $ad->description1,
                        "desc2"     =>  $ad->description2,
                        "display"   =>  $ad->displayUrl,
                        "url"       =>  $ad->url    );
        $output[] = $json;
    }
    $output = json_encode($output);
    echo "ads.push({keyword:\"$keyword\",ads:$output});";
}
// headline = #0000cc
// desc1 = #333333
// desc2 = #333333
// url = #0e8f0e
?>
function show(obj,id) {
    obj.style.fontWeight = "bold";
    var ad = ads[id], i;
    var html = "<br/>";
    html += "keyword = <b>"+ad.keyword+"</b><br/><br/>";
    for (i=0; i<ad.ads.length; i++) {
        var this_ad = ad.ads[i];
        html += "<u>Ad #"+(i+1)+":</u><br/>";
        html += '<div style="float:left;width:250px"><a target="_blank" href="'+this_ad.url+'" style="text-decoration:none;color:#0000cc">'+this_ad.headline+'</a></div><div style="float:left"><i>('+this_ad.headline.length+')</i></div><div style="clear:both"></div>';
        html += '<div style="float:left;width:250px"><a target="_blank" href="'+this_ad.url+'" style="width:200px;text-decoration:none;color:#333333">'+this_ad.desc1+'</a></div><div style="float:left"><i>('+this_ad.desc1.length+')</i></div><div style="clear:both"></div>';
        html += '<div style="float:left;width:250px"><a target="_blank" href="'+this_ad.url+'" style="width:200px;text-decoration:none;color:#333333">'+this_ad.desc2+'</a></div><div style="float:left"><i>('+this_ad.desc2.length+')</i></div><div style="clear:both"></div>';
        html += '<div style="float:left;width:250px"><a target="_blank" href="'+this_ad.url+'" style="width:200px;text-decoration:none;color:#0e8f0e">'+this_ad.display+'</a></div><div style="float:left"><i>('+this_ad.display.length+')</i><br/></div><div style="clear:both"></div>';
        html += '<br/>';
    }
    html += "<br/>";
    document.getElementById('box'+id).innerHTML = html;
}

function hide(id) {
    var obj = document.getElementById('box'+id).previousSibling;
    obj.style.fontWeight = "";
    document.getElementById('box'+id).innerHTML = "";
}

for (i=0; i<ads.length; i++) {
    document.write('<a onmouseover="show(this,'+i+')" href="javascript:hide('+i+')">'+ads[i].keyword+'</a><div id="box'+i+'"></div>');
}
</script>
<div style="position:fixed;top:20px;right:20px"><button onclick="window.open('builder2-fashion.php?export=csv')">Download CSV</button></div>