<?php
// change_bids_test.php - test script that tries to modify bids in a specific campaign based on data from reporting & data from adwords account (bid etc.)
// ====================================================================================================================================================================================
require_once "../includes/adwordsapi.class.php";                                // adwords class that is used for operations using Adwords API on the MCC account
require_once "../includes/meekrodb.2.2.class.php";                              // meekro class that is used for DB operations
$adwords = new adwordsapi("6539967406");
$local_db = new MeekroDB("localhost", "root", "Miki123Abc", "adwords_bidder");
$file = "Furniture-exact-2.csv";
// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function    english_name($name) {                                               // english_name = helper function that converts a column name to a php kind of column name
    $output = "";
    $temp_name = strtolower($name);                                             // make the entire string lower case
    for ($i=0; $i<strlen($temp_name); $i++) {                                   // move over all the characters of the column name
        $ch = $temp_name[$i];                                                   // take the current char
        if ((($ch>='a')&&($ch<='z'))||(($ch>='0')&&($ch<='9'))) {               // allow only a-z and 0-9 characters
            $output .= $ch;
        } else if ($ch==' ') {                                                  // and convert spaces to _
            $output .= "_";
        }
    }
    return $output;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function add_adgroup($campaign_id=null, $data=null) {
    global $adwords;
    if ((!$data)||(!$campaign_id)) return;
    $adGroupService = $adwords->get_service('AdGroupService');
    $adGroup = new AdGroup();                                                   // create new adgroup
    $adGroup->campaignId = $campaign_id;
    $adGroup->name = $data['adgroup_name'];
    // Set bids (required).
    $bid = new CpcBid();
    $bid->bid =  new Money($data['cpc'] * 1000000);
    $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
    $biddingStrategyConfiguration->bids[] = $bid;
    $adGroup->biddingStrategyConfiguration = $biddingStrategyConfiguration;
    $adGroup->status = 'ENABLED';
    // Create operation.
    $operation = new AdGroupOperation();
    $operation->operand = $adGroup;
    $operation->operator = 'ADD';
    $operations[] = $operation;
    // run the operation
    $result = $adGroupService->mutate($operations);
    // Display result.
    return $result;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function add_keyword($data=null) {
    global $adwords;
    if (!$data) return;
    $adGroupCriterionService = $adwords->get_service('AdGroupCriterionService');
    $keyword = new Keyword();
    $keyword->text = $data['keyword'];
    $keyword->matchType = $data['match_type'];
    // Create biddable ad group criterion.
    $adGroupCriterion = new BiddableAdGroupCriterion();
    $adGroupCriterion->adGroupId = $data['adgroup_id'];
    $adGroupCriterion->criterion = $keyword;
    // Set additional settings (optional).
    $adGroupCriterion->userStatus = 'ACTIVE';
    // Create operation.
    $operation = new AdGroupCriterionOperation();
    $operation->operand = $adGroupCriterion;
    $operation->operator = 'ADD';
    $operations[] = $operation;
    $result = $adGroupCriterionService->mutate($operations);
    return $result;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_ad1($adgroup_id, $keyword) {
    global $adwords;
    $words = explode(" ",$keyword);                                             // take keyword/s
    foreach ($words as $c=>$word) $words[$c][0] = strtoupper($word[0]);         // make first word in each keyword UPCASE
    $words_keyword = implode(" ",$words);
    $words_display = implode("-",$words);
    $words_query = implode("%20",$words);
    // ========================================================================
    $textAd = new TextAd();
    $headline = "Wanted: $words_keyword";                       if (strlen($headline)>25) $headline = "Wanted: Furniture Deals";
    $textAd->headline = $headline;                                              // 25 chars
    $textAd->description1 = '"I Have Been Searching for a Better';              // 35 chars
    $desc2 = 'Price for '.$words_keyword.'. Found It!"';        if (strlen($desc2)>35) $desc2 = 'Price for Furniture. Found It!"';
    $textAd->description2 = $desc2;                                            // 35 chars
    $url = "$words_display.FindSimilar.com";                    if (strlen($url)>35) $url = "Furniture.FindSimilar.com";
    $textAd->displayUrl = $url;                                                // 35 chars
    $textAd->url = "http://www.findsimilar.com/search.php?query=$words_query&cid=4778";
    // Create ad group ad.
    $adGroupAd = new AdGroupAd();
    $adGroupAd->adGroupId = $adgroup_id;
    $adGroupAd->ad = $textAd;
    // Set additional settings (optional).
    $adGroupAd->status = 'ENABLED';
    return $adGroupAd;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_ad2($adgroup_id, $keyword) {
    global $adwords;
    $words = explode(" ",$keyword);                                             // take keyword/s
    foreach ($words as $c=>$word) $words[$c][0] = strtoupper($word[0]);         // make first word in each keyword UPCASE
    $words_keyword = implode(" ",$words);
    $words_display = implode("-",$words);
    $words_query = implode("%20",$words);
    // ========================================================================
    $textAd = new TextAd();
    $headline = "$words_keyword- Outlet";           if (strlen($headline)>25) $headline = $words_keyword;           if (strlen($headline)>25) $headline = "Furniture- Outlet";
    $textAd->headline = $headline;                                              // 25 chars
    $desc1 = '"I Just Bought '.$words_keyword;      if (strlen($desc1)>35) $desc1 = '"I Bought '.$words_keyword;    if (strlen($desc1)>35) $desc1 = '"I Just Bought New Furniture';
    $textAd->description1 = $desc1;
    $textAd->description2 = '& Paid Less Than You Are Going To"';               // 35 chars
    $url = "$words_display.FindSimilar.com";        if (strlen($url)>35) $url = "Furniture.FindSimilar.com";
    $textAd->displayUrl = $url;                                                // 35 chars
    $textAd->url = "http://www.findsimilar.com/search.php?query=$words_query&cid=4778";
    // Create ad group ad.
    $adGroupAd = new AdGroupAd();
    $adGroupAd->adGroupId = $adgroup_id;
    $adGroupAd->ad = $textAd;
    // Set additional settings (optional).
    $adGroupAd->status = 'ENABLED';
    return $adGroupAd;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_ad3($adgroup_id, $keyword) {
    global $adwords;
    $flag = false;
    $tkeyword = ($keyword[strlen($keyword)-1]=="s")?$keyword:"{$keyword}s";
    $words = explode(" ",$tkeyword);                                            // take keyword/s
    foreach ($words as $c=>$word) $words[$c][0] = strtoupper($word[0]);         // make first word in each keyword UPCASE
    $words_keyword = implode(" ",$words);
    $words_display = implode("-",$words);
    $words_query = implode("%20",$words);
    // ========================================================================
    $textAd = new TextAd();
    $headline = "Explore 100+ ".$words_keyword;
    if (strlen($headline)>25) { $flag=true; $headline = "Explore 100+ Sales"; }
    $textAd->headline = $headline;                                              // 25 chars
    $textAd->description1 = "Yes, There is Always a Better Deal";
    if ($flag) $desc2 = "for this -Take a Look";
    else {
        $desc2 = "for $words_keyword -Take a Look";     
        if (strlen($desc2)>35) $desc2 = "for this -Take a Look";
    }
    $textAd->description2 = $desc2;                                             // 35 chars
    $url = "$words_display.FindSimilar.com";        if (strlen($url)>35) $url = "Furniture.FindSimilar.com";
    $textAd->displayUrl = $url;                                                 // 35 chars
    $textAd->url = "http://www.findsimilar.com/search.php?query=$words_query&cid=4778";
    // Create ad group ad.
    $adGroupAd = new AdGroupAd();
    $adGroupAd->adGroupId = $adgroup_id;
    $adGroupAd->ad = $textAd;
    // Set additional settings (optional).
    $adGroupAd->status = 'ENABLED';
    return $adGroupAd;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_virtual_ad1($keyword) {
    $words = explode(" ",$keyword);                                             // take keyword/s
    foreach ($words as $c=>$word) $words[$c][0] = strtoupper($word[0]);         // make first word in each keyword UPCASE
    $words_keyword = implode(" ",$words);
    $words_display = implode("-",$words);
    $words_query = implode("%20",$words);
    // ========================================================================
    $textAd = new stdClass();
    $headline = "Wanted: $words_keyword";                       if (strlen($headline)>25) $headline = "Wanted: Furniture Deals";
    $textAd->headline = $headline;                                              // 25 chars
    $textAd->description1 = '"I Have Been Searching for a Better';              // 35 chars
    $desc2 = 'Price for '.$words_keyword.'. Found It!"';        if (strlen($desc2)>35) $desc2 = 'Price for Furniture. Found It!"';
    $textAd->description2 = $desc2;                                            // 35 chars
    $url = "$words_display.FindSimilar.com";                    if (strlen($url)>35) $url = "Furniture.FindSimilar.com";
    $textAd->displayUrl = $url;                                                // 35 chars
    $textAd->url = "http://www.findsimilar.com/search.php?query=$words_query&cid=4778";
    // Create ad group ad.
    return $textAd;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_virtual_ad2($keyword) {
    $words = explode(" ",$keyword);                                             // take keyword/s
    foreach ($words as $c=>$word) $words[$c][0] = strtoupper($word[0]);         // make first word in each keyword UPCASE
    $words_keyword = implode(" ",$words);
    $words_display = implode("-",$words);
    $words_query = implode("%20",$words);
    // ========================================================================
    $textAd = new stdClass();
    $headline = "$words_keyword- Outlet";           if (strlen($headline)>25) $headline = $words_keyword;           if (strlen($headline)>25) $headline = "Furniture- Outlet";
    $textAd->headline = $headline;                                              // 25 chars
    $desc1 = '"I Just Bought '.$words_keyword;      if (strlen($desc1)>35) $desc1 = '"I Bought '.$words_keyword;    if (strlen($desc1)>35) $desc1 = '"I Just Bought New Furniture';
    $textAd->description1 = $desc1;
    $textAd->description2 = '& Paid Less Than You Are Going To"';               // 35 chars
    $url = "$words_display.FindSimilar.com";        if (strlen($url)>35) $url = "Furniture.FindSimilar.com";
    $textAd->displayUrl = $url;                                                // 35 chars
    $textAd->url = "http://www.findsimilar.com/search.php?query=$words_query&cid=4778";
    // Create ad group ad.
    return $textAd;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function create_virtual_ad3($keyword) {
    $flag = false;
    $tkeyword = ($keyword[strlen($keyword)-1]=="s")?$keyword:"{$keyword}s";
    $words = explode(" ",$tkeyword);                                            // take keyword/s
    foreach ($words as $c=>$word) $words[$c][0] = strtoupper($word[0]);         // make first word in each keyword UPCASE
    $words_keyword = implode(" ",$words);
    $words_display = implode("-",$words);
    $words_query = implode("%20",$words);
    // ========================================================================
    $textAd = new stdClass();
    $headline = "Explore 100+ ".$words_keyword;
    if (strlen($headline)>25) { $flag=true; $headline = "Explore 100+ Sales"; }
    $textAd->headline = $headline;                                              // 25 chars
    $textAd->description1 = "Yes, There is Always a Better Deal";
    if ($flag) $desc2 = "for this -Take a Look";
    else {
        $desc2 = "for $words_keyword -Take a Look";     
        if (strlen($desc2)>35) $desc2 = "for this -Take a Look";
    }
    $textAd->description2 = $desc2;                                             // 35 chars
    $url = "$words_display.FindSimilar.com";        if (strlen($url)>35) $url = "Furniture.FindSimilar.com";
    $textAd->displayUrl = $url;                                                 // 35 chars
    $textAd->url = "http://www.findsimilar.com/search.php?query=$words_query&cid=4778";
    // Create ad group ad.
    return $textAd;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function add_ad($adgroup_id=null, $data=null) {
    global $adwords;
    if ((!$data)||(!$adgroup_id)) return;
    $adGroupAdService = $adwords->get_service('AdGroupAdService');
    $adGroupAd1 = create_ad1($adgroup_id, $data['keyword']);
    $adGroupAd2 = create_ad2($adgroup_id, $data['keyword']);
    $adGroupAd3 = create_ad3($adgroup_id, $data['keyword']);
    // Create operation.
    $operation1 = new AdGroupAdOperation();
    $operation1->operand = $adGroupAd1;
    $operation1->operator = 'ADD';
    $operation2 = new AdGroupAdOperation();
    $operation2->operand = $adGroupAd2;
    $operation2->operator = 'ADD';
    $operation2 = new AdGroupAdOperation();
    $operation2->operand = $adGroupAd3;
    $operation2->operator = 'ADD';
    $operations[] = $operation1;
    $operations[] = $operation2;
    $result = $adGroupAdService->mutate($operations);
    return $result;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function add_virtual_ad($data=null) {
    $ad1 = create_virtual_ad1($data['keyword']);
    $ad2 = create_virtual_ad2($data['keyword']);
    $ad3 = create_virtual_ad3($data['keyword']);
    return array($ad1,$ad2,$ad3);
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ====================================================================================================================================================================================
$csv_data = file_get_contents($file);                                           // part 1 - open Michal's CSV file with keywords
$csv_lines = array();
$csv_output = array();
$lines = explode("\n",$csv_data);
foreach ($lines as $i=>$line) if (trim($line)!="") $csv_lines[] = explode(",",$line);
$column_names = $csv_lines[0];
foreach ($column_names as $i=>$name) $column_names[$i] = trim(english_name($name));
for ($i=1; $i<count($csv_lines); $i++) {
    $arr = array();
    for ($c=0; $c<count($column_names); $c++) {
        $data = $csv_lines[$i][$c];
        $arr[$column_names[$c]] = $data;
    }
    $csv_output[] = $arr;
}
$campaign = $local_db->query("SELECT * FROM campaigns WHERE campaign_id=203688202");    // take all information from existing campaign Michal created 4778_Furniture Exact -UK
$campaign = $campaign[0];
$adwords->switch_user("7508355071");                                            // switch to Findsimilar campaign now..
$campaign_id = $campaign['campaign_id'];
?>
<style>
body {
    font-family:arial;
    font-size:12px;
}
</style>
<script type="text/javascript">
var ads = new Array();
<?php
foreach ($csv_output as $i=>$line) {
    // NOW NEED TO PERFORM A BULK OPERATION
    $adgroup_name = $line['ad_group'];
    $keyword = $line['keyword'];
    $citerion = $line['criterion_type'];
    //$cpc = $line['max_cpc'];
    $cpc = "0.20";                                                              // changed manually by Miki to fit Michal's settings
    //$data = array("adgroup_name"=>$keyword, "cpc"=>$cpc);
    //$arr = add_adgroup($campaign_id, $data);
    //$adgroup_id = $arr->value[0]->id;                                           // get the ID of the newly created adgroup
    //$adgroup_id = "15079819882";                                                // adgroup_id for "sofa"
    //$data = array("match_type"=>"EXACT", "keyword"=>$keyword, "adgroup_id"=>$adgroup_id);
    //$arr2 = add_keyword($data);
    //$arr3 = add_ad($adgroup_id, array("keyword"=>$keyword));
    $arr = add_virtual_ad(array("keyword"=>$keyword));
    $output = array();
    foreach ($arr as $n=>$ad) {
        $json = array(  "headline"  =>  $ad->headline,
                        "desc1"     =>  $ad->description1,
                        "desc2"     =>  $ad->description2,
                        "display"   =>  $ad->displayUrl,
                        "url"       =>  $ad->url    );
        $output[] = $json;
    }
    $output = json_encode($output);
    echo "ads.push({keyword:\"$keyword\",ads:$output});";
}
// headline = #0000cc
// desc1 = #333333
// desc2 = #333333
// url = #0e8f0e
?>
function show(obj,id) {
    obj.style.fontWeight = "bold";
    var ad = ads[id], i;
    var html = "<br/>";
    html += "keyword = <b>"+ad.keyword+"</b><br/><br/>";
    for (i=0; i<ad.ads.length; i++) {
        var this_ad = ad.ads[i];
        html += "<u>Ad #"+(i+1)+":</u><br/>";
        html += '<div style="float:left;width:250px"><a href="'+this_ad.url+'" style="text-decoration:none;color:#0000cc">'+this_ad.headline+'</a></div><div style="float:left"><i>('+this_ad.headline.length+')</i></div><div style="clear:both"></div>';
        html += '<div style="float:left;width:250px"><a href="'+this_ad.url+'" style="width:200px;text-decoration:none;color:#333333">'+this_ad.desc1+'</a></div><div style="float:left"><i>('+this_ad.desc1.length+')</i></div><div style="clear:both"></div>';
        html += '<div style="float:left;width:250px"><a href="'+this_ad.url+'" style="width:200px;text-decoration:none;color:#333333">'+this_ad.desc2+'</a></div><div style="float:left"><i>('+this_ad.desc2.length+')</i></div><div style="clear:both"></div>';
        html += '<div style="float:left;width:250px"><a href="'+this_ad.url+'" style="width:200px;text-decoration:none;color:#0e8f0e">'+this_ad.display+'</a></div><div style="float:left"><i>('+this_ad.display.length+')</i><br/></div><div style="clear:both"></div>';
        html += '<br/>';
    }
    html += "<br/>";
    document.getElementById('box'+id).innerHTML = html;
}

function hide(id) {
    var obj = document.getElementById('box'+id).previousSibling;
    obj.style.fontWeight = "";
    document.getElementById('box'+id).innerHTML = "";
}

for (i=0; i<ads.length; i++) {
    document.write('<a onmouseover="show(this,'+i+')" href="javascript:hide('+i+')">'+ads[i].keyword+'</a><div id="box'+i+'"></div>');
}
</script>