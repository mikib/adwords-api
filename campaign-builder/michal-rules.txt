Wanted: {KEYWORD}							<-- if too long "Wanted: Furniture Deals"  (headline)
"I Have Found a Better Price for
{KEYWORD}!"									<-- if too long - Furniture
{KEYWORD}.FindSimilar.com					<-- if keyword if two words - seperate with "-" , if too long -> Furniture


{KEYWORD}- Outlet							<-- if too long, remove Outlet, if still long : "Furniture- Outlet"
"I Just Bought {KEYWORD}					<-- if long, remove "Just", if still long: "I Just Bought New Furniture
& Paid Less Than You Are Going To"
{KEYWORD}.FindSimilar.com					<-- same logic as above


