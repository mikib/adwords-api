AdGroupCriterionReturnValue Object
(
    [value] => Array
        (
            [0] => BiddableAdGroupCriterion Object
                (
                    [userStatus] => ACTIVE
                    [systemServingStatus] => ELIGIBLE
                    [approvalStatus] => PENDING_REVIEW
                    [disapprovalReasons] =>
                    [destinationUrl] =>
                    [experimentData] =>
                    [firstPageCpc] =>
                    [topOfPageCpc] =>
                    [qualityInfo] =>
                    [biddingStrategyConfiguration] => BiddingStrategyConfiguration Object
                        (
                            [biddingStrategyId] =>
                            [biddingStrategyName] =>
                            [biddingStrategyType] => MANUAL_CPC
                            [biddingStrategySource] => CAMPAIGN
                            [biddingScheme] => ManualCpcBiddingScheme Object
                                (
                                    [enhancedCpcEnabled] =>
                                    [BiddingSchemeType] => ManualCpcBiddingScheme
                                    [_parameterMap:BiddingScheme:private] => Array
                                        (
                                            [BiddingScheme.Type] => BiddingSchemeType
                                        )

                                )

                            [bids] => Array
                                (
                                    [0] => CpcBid Object
                                        (
                                            [bid] => Money Object
                                                (
                                                    [microAmount] => 200000
                                                    [ComparableValueType] => Money
                                                    [_parameterMap:ComparableValue:private] => Array
                                                        (
                                                            [ComparableValue.Type] => ComparableValueType
                                                        )

                                                )

                                            [contentBid] =>
                                            [cpcBidSource] => ADGROUP
                                            [BidsType] => CpcBid
                                            [_parameterMap:Bids:private] => Array
                                                (
                                                    [Bids.Type] => BidsType
                                                )

                                        )

                                    [1] => CpmBid Object
                                        (
                                            [bid] => Money Object
                                                (
                                                    [microAmount] => 250000
                                                    [ComparableValueType] => Money
                                                    [_parameterMap:ComparableValue:private] => Array
                                                        (
                                                            [ComparableValue.Type] => ComparableValueType
                                                        )

                                                )

                                            [cpmBidSource] => ADGROUP
                                            [BidsType] => CpmBid
                                            [_parameterMap:Bids:private] => Array
                                                (
                                                    [Bids.Type] => BidsType
                                                )

                                        )

                                )

                        )

                    [bidModifier] =>
                    [adGroupId] => 15079819882
                    [criterionUse] =>
                    [criterion] => Keyword Object
                        (
                            [text] => sofa
                            [matchType] => EXACT
                            [id] => 14916080
                            [type] => KEYWORD
                            [CriterionType] => Keyword
                            [_parameterMap:Criterion:private] => Array
                                (
                                    [Criterion.Type] => CriterionType
                                )

                        )

                    [forwardCompatibilityMap] =>
                    [AdGroupCriterionType] => BiddableAdGroupCriterion
                    [_parameterMap:AdGroupCriterion:private] => Array
                        (
                            [AdGroupCriterion.Type] => AdGroupCriterionType
                        )

                )

        )

    [partialFailureErrors] =>
    [ListReturnValueType] => AdGroupCriterionReturnValue
    [_parameterMap:ListReturnValue:private] => Array
        (
            [ListReturnValue.Type] => ListReturnValueType
        )

)