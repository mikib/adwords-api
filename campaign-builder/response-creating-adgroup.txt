    [value] => Array
        (
            [0] => AdGroup Object
                (
                    [id] => 15079819882
                    [campaignId] => 203688202
                    [campaignName] => 4778_Furniture Exact -UK
                    [name] => sofa
                    [status] => ENABLED
                    [settings] =>
                    [experimentData] =>
                    [forwardCompatibilityMap] =>
                    [biddingStrategyConfiguration] => BiddingStrategyConfiguration Object
                        (
                            [biddingStrategyId] =>
                            [biddingStrategyName] =>
                            [biddingStrategyType] => MANUAL_CPC
                            [biddingStrategySource] => CAMPAIGN
                            [biddingScheme] => ManualCpcBiddingScheme Object
                                (
                                    [enhancedCpcEnabled] =>
                                    [BiddingSchemeType] => ManualCpcBiddingScheme
                                    [_parameterMap:BiddingScheme:private] => Array
                                        (
                                            [BiddingScheme.Type] => BiddingSchemeType
                                        )

                                )

                            [bids] => Array
                                (
                                    [0] => CpcBid Object
                                        (
                                            [bid] => Money Object
                                                (
                                                    [microAmount] => 260000
                                                    [ComparableValueType] => Money
                                                    [_parameterMap:ComparableValue:private] => Array
                                                        (
                                                            [ComparableValue.Type] => ComparableValueType
                                                        )

                                                )

                                            [contentBid] =>
                                            [cpcBidSource] => ADGROUP
                                            [BidsType] => CpcBid
                                            [_parameterMap:Bids:private] => Array
                                                (
                                                    [Bids.Type] => BidsType
                                                )

                                        )

                                    [1] => CpaBid Object
                                        (
                                            [bid] => Money Object
                                                (
                                                    [microAmount] => 10000
                                                    [ComparableValueType] => Money
                                                    [_parameterMap:ComparableValue:private] => Array
                                                        (
                                                            [ComparableValue.Type] => ComparableValueType
                                                        )

                                                )

                                            [BidsType] => CpaBid
                                            [_parameterMap:Bids:private] => Array
                                                (
                                                    [Bids.Type] => BidsType
                                                )

                                        )

                                    [2] => CpmBid Object
                                        (
                                            [bid] => Money Object
                                                (
                                                    [microAmount] => 250000
                                                    [ComparableValueType] => Money
                                                    [_parameterMap:ComparableValue:private] => Array
                                                        (
                                                            [ComparableValue.Type] => ComparableValueType
                                                        )

                                                )

                                            [cpmBidSource] => ADGROUP
                                            [BidsType] => CpmBid
                                            [_parameterMap:Bids:private] => Array
                                                (
                                                    [Bids.Type] => BidsType
                                                )

                                        )

                                )

                        )

                    [contentBidCriterionTypeGroup] =>
                )

        )

    [partialFailureErrors] =>
    [ListReturnValueType] => AdGroupReturnValue
    [_parameterMap:ListReturnValue:private] => Array
        (
            [ListReturnValue.Type] => ListReturnValueType
        )

)