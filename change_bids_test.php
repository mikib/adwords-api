<?php
// change_bids_test.php - test script that tries to modify bids in a specific campaign based on data from reporting & data from adwords account (bid etc.)
// ====================================================================================================================================================================================
require_once "adwordsapi.class.php";
require_once "meekrodb.2.2.class.php";
$adwords = new adwordsapi();
$local_db = new MeekroDB("localhost", "root", "Miki123Abc", "adwords_bidder");
$reporting_db = new MeekroDB("50.22.193.151", "ibario", "ajsm28zjqhwa193", "ibario");
// ====================================================================================================================================================================================
$client_id = 6092654773;                                                        // the client ID we test on : Free Codec
$campaign_names = array("4258_CDC-DCO - FR","4258_CDC-Content(CPA) - NL");      // the campaign names we test on
$cid = 4258;
// ====================================================================================================================================================================================
require_once "include/bids_test.php";                                           // contains additional functions taken from the reporting platform
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_test_campaigns() {
    global $local_db,$client_id,$campaign_names;
    $campaigns_indexed = array();                                               // save the campaigns in an indexed array - where key = campaign_id, for easier locating of adgroups
    $campaigns = $local_db->query("SELECT * FROM campaigns WHERE client_id=$client_id AND campaign_name IN ('".implode("','",$campaign_names)."')");
    foreach ($campaigns as $i=>$campaign) {
        $campaigns_indexed[$campaign['campaign_id']] = $campaign;
        $campaigns_indexed[$campaign['campaign_id']]['adgroups'] = array();
    }
    return $campaigns_indexed;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_test_adgroups(&$campaigns) {
    global $local_db,$campaigns;
    $campaign_ids = array();
    foreach ($campaigns as $key=>$campaign) $campaign_ids[] = $key;             // extract the campaign_ids from the indexed array
    $adgroups = $local_db->query("SELECT * FROM adgroups WHERE campaign_id IN (".implode(",",$campaign_ids).")");
    foreach ($adgroups as $i=>$adgroup) $campaigns[$adgroup['campaign_id']]['adgroups'][] = $adgroup;       // store the adgroups under the campaign this time
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_cid_totals($cid,$date=null) {
    global $reporting_db;
    $from_date = ($date)?date("Y-m-d",$date):date("Y-m-d",time()-86400);
    $cid_totals = $reporting_db->query("SELECT * from cid_channels_totals WHERE cid=$cid AND createdate='$from_date'");
    $channels = explode(",",$cid_totals[0]['channels']);
    $cid_totals[0]['channel_ids'] = $channels;
    return $cid_totals;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_reporting_data($cid_totals,$date=null) {
    global $reporting_db,$config_admin_installs,$config_admin_revenues,$config_sales_components,$config_search_components,$config_secondary_components,$config_special_cpa_components,$config_special_cpa_components_revenues;
    $output = array();
    $channels = $cid_totals['channel_ids'];
    $from_date = ($date)?date("Y-m-d",$date):date("Y-m-d",time()-86400);
    $query = "SELECT country,SUM(amount*factor/100) AS revenue FROM stats ".
             "WHERE codename NOT like '%revenues%' ".
             "AND codename IN ('".implode("','",$config_admin_revenues)."') ".
             "AND channel_id IN ('".implode("','",$channels)."') ".
             "AND createdate='$from_date' GROUP by country";
    $data = $reporting_db->query($query);
    foreach ($data as $i=>$array) $output[$array['country']]['revenue'] = $array['revenue'];   // organize revenue sums by country (geo)
    $query = "SELECT country, SUM(clicks) AS clicks, SUM(installs) AS pixels, SUM(amount*factor/100) AS spend FROM stats ".
             "WHERE channel_id IN ('".(implode("','", $channels))."') ".
             "AND codename NOT IN ('".implode("','", $config_sales_components)."') ".
             "AND codename NOT IN ('".implode("','", $config_search_components)."') ".
             "AND codename NOT IN ('".implode("','", $config_secondary_components)."') ".
             "AND codename NOT IN ('".implode("','", $config_special_cpa_components)."') ".
             "AND codename NOT IN ('".implode("','", $config_special_cpa_components_revenues)."') ".
             "AND codename NOT LIKE '%revenues%' ".
             "AND createdate='$from_date' GROUP by country";
    $data = $reporting_db->query($query);
    foreach ($data as $i=>$array) {
        $output[$array['country']]['clicks'] = $array['clicks'];
        $output[$array['country']]['pixels'] = $array['pixels'];
        $output[$array['country']]['spend'] = $array['spend'];
    }
    return $output;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get_spends_from_db($campaign_id,$date=null) {
    global $local_db;
    $from_date = ($date)?date("Y-m-d",$date):date("Y-m-d",time()-86400);
    $query = "SELECT SUM(cost) AS cost FROM adgroups_stats WHERE campaign_id=$campaign_id AND date_time LIKE '$from_date%'";
    $data = $local_db->query($query);
    return $data[0]['cost'];
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function read_key($prompt) {
    $stdin = fopen("php://stdin", "r");
    print($prompt);
    $text = fgets($stdin);
    fclose($stdin);
    return $text;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function update_bids($campaign, $cpabid) {
    global $adwords;
    $adwords->switch_user($campaign['client_id']);
    $adgroups = array();
    foreach ($campaign['adgroups'] as $i=>$adgroup) {
        $adgroups[] = array("adgroup_id"=>$adgroup['adgroup_id'],"bid"=>"cpa","amount"=>$cpabid);
        if ($i>1) break;
    }
    $adwords->update_adgroups($adgroups);
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function change_bids($revenue, $campaign_name,$geo) {
    global $local_db, $reporting_db, $campaigns, $cid;
    foreach ($campaigns as $key=>$campaign) {
        if ($campaign['campaign_name']==$campaign_name) {
            $spend = get_spends_from_db($campaign['campaign_id']);
            print_r($revenue);
            echo "CID=$cid, Campaign Name=$campaign_name, Spend=".($spend/1000000);
            $cpa = (($spend/1000000)/($revenue['pixels']));
            $target_cpa = $cpa / 1.3;
            echo ", CPA = $cpa , setting CPA bid to $target_cpa\r\n";
            $string = read_key("Enter 'yes' to continue: ");
            if (substr($string,0,3)=="yes") update_bids($campaign,$target_cpa);
        }
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function collect_config_admin($type) {
    global $reporting_db;
    $output = array();
    $data = $reporting_db->query("SELECT * FROM config_codenames_definition WHERE admin_type='$type'");
    foreach ($data as $i=>$arr) $output[] = $arr['codename'];
    return $output;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function collect_config_component($type) {
    global $reporting_db;
    $output = array();
    $data = $reporting_db->query("SELECT * FROM config_codenames_definition WHERE component_type='$type'");
    foreach ($data as $i=>$arr) $output[] = $arr['codename'];
    return $output;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* the two test campaigns I test on now:
 * 
 * 4258_CDC-DCO - FR
 * 4258_CDC-Content(CPA) - NL
 * 
 * Both belong to client Free Codec (6092654773)
 */
// step 1 : load the local data we have stored for the campaign & adgroups locally
$config_admin_revenues = collect_config_admin('revenue');                       // configuration
$config_sales_components = collect_config_component('sales');                       // configuration
$config_search_components = collect_config_component('search');                     // configuration
$config_secondary_components = collect_config_component('secondary');               // configuration

$campaigns = get_test_campaigns();
get_test_adgroups($campaigns);                                                  // this will save the adgroups under the campaign
// step 2 : load the reporting data stored for this campaign / adgroup on reporting.installbrain.com
$cid_totals = get_cid_totals($cid);
$data = get_reporting_data($cid_totals[0]);
change_bids($data['Netherlands'],$campaign_names[1],"Netherlands");
?>